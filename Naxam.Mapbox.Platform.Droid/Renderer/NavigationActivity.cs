﻿//using System;
//using System.Collections.Generic;
//using Android.App;
//using Android.Content;
//using Android.Content.PM;
//using Android.Locations;
//using Android.OS;
//using Android.Support.Design.Widget;
//using Android.Support.V7.App;
//using Android.Views;
//using Android.Widget;
//using Com.Mapbox.Api.Directions.V5.Models;
//using Com.Mapbox.Mapboxsdk;
//using Com.Mapbox.Mapboxsdk.Annotations;
//using Com.Mapbox.Mapboxsdk.Camera;
//using Com.Mapbox.Mapboxsdk.Exceptions;
//using Com.Mapbox.Mapboxsdk.Geometry;
//using Com.Mapbox.Mapboxsdk.Maps;
//using Com.Mapbox.Mapboxsdk.Plugins.Locationlayer;
//using Com.Mapbox.Services.Android.Navigation.UI.V5;
//using Com.Mapbox.Services.Android.Navigation.UI.V5.Route;
//using Com.Mapbox.Services.Android.Navigation.V5.Navigation;
//using Com.Mapbox.Services.Android.Telemetry.Location;
////using Com.Mapbox.Services.Android.Telemetry.Location;
//using Java.Lang;
//using Square.Retrofit2;

//namespace Naxam.Mapbox.Platform.Droid.Renderer
//{
//    [Activity(Label = "Navigation", Theme = "@style/Theme.AppCompat", WindowSoftInputMode = SoftInput.StateHidden, ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize)]
//    internal class NavigationActivity : AppCompatActivity, IOnMapReadyCallback,
//        ILocationEngineListener, ICallback
//    {
//        private const string LatitudeTag = nameof(LatitudeTag);
//        private const string LongitudeTag = nameof(LongitudeTag);
//        private const string NameTag = nameof(NameTag);

//        private const int CameraAnimationDuration = 1000;

//        private LocationLayerPlugin _locationLayer;
//        private LocationEngine _locationEngine;
//        private GoogleLocationEngine _googleLocationEngine;
//        private NavigationMapRoute _mapRoute;
//        private MapboxMap _mapboxMap;

//        private MapView _mapView;
//        private Button _launchRouteButton;
//        private ProgressBar _loadingProgressBar;

//        private Marker _currentMarker;
//        private Com.Mapbox.Geojson.Point _currentLocation;
//        private Com.Mapbox.Geojson.Point _destination;
//        private DirectionsRoute _route;

//        private bool _locationFound;
//        private double _latitude;
//        private double _longitude;
//        private string _name;

//        public static Intent CreateStartIntent(Context context, double latitude, double longitude, string name)
//        {
//            return new Intent(context, typeof(NavigationActivity))
//                .PutExtra(LatitudeTag, latitude)
//                .PutExtra(LongitudeTag, longitude)
//                .PutExtra(NameTag, name);
//        }

//        protected override void OnCreate(Bundle savedInstanceState)
//        {
//            Mapbox.GetInstance(this, "pk.eyJ1IjoiZGVuZWF0aCIsImEiOiJjajh0bWYxenIwcDFuMnJxemE3Y2VpdndmIn0.zIFjC_o9v_rDUxgvZjkkQQ");

//            base.OnCreate(savedInstanceState);

//            _latitude = Intent.GetDoubleExtra(LatitudeTag, 0);
//            _longitude = Intent.GetDoubleExtra(LongitudeTag, 0);
//            _name = Intent.GetStringExtra(NameTag);

//            Title = _name;

//            SetContentView(Resource.Layout.screen_navigation);

//            _mapView = FindViewById<MapView>(Resource.Id.mapView);
//            _launchRouteButton = FindViewById<Button>(Resource.Id.launchRouteButton);
//            _loadingProgressBar = FindViewById<ProgressBar>(Resource.Id.loadingProgressBar);

//            _mapView.OnCreate(savedInstanceState);
//            _mapView.GetMapAsync(this);

//            _launchRouteButton.Click += (sender, args) => LaunchNavigationWithRoute();
//        }

//        protected override void OnStart()
//        {
//            base.OnStart();

//            _mapView.OnStart();

//            //InitLocationEngine();
//            //InitLocationLayer();
//            //InitMapRoute();
//            _locationLayer?.OnStart();
//        }

//        protected override void OnResume()
//        {
//            base.OnResume();

//            _mapView.OnResume();

//            //_locationEngine?.AddLocationEngineListener(this);
//            //if (_locationEngine?.IsConnected ?? false)
//            //_locationEngine.Activate();

//            _googleLocationEngine?.AddLocationEngineListener(this);
//            if (_googleLocationEngine?.IsConnected ?? false)
//                _googleLocationEngine.Activate();
//        }

//        protected override void OnPause()
//        {
//            base.OnPause();

//            _mapView.OnPause();

//            //_locationEngine?.RemoveLocationEngineListener(this);
//            _googleLocationEngine?.RemoveLocationEngineListener(this);
//        }

//        public override void OnLowMemory()
//        {
//            base.OnLowMemory();

//            _mapView.OnLowMemory();
//        }

//        protected override void OnStop()
//        {
//            base.OnStop();

//            _mapView.OnStop();

//            _locationLayer?.OnStop();
//        }

//        protected override void OnDestroy()
//        {
//            base.OnDestroy();

//            _mapView.OnDestroy();

//            //_locationEngine?.RemoveLocationUpdates();
//            //_locationEngine?.Deactivate();
//            _googleLocationEngine?.RemoveLocationUpdates();
//            _googleLocationEngine?.Deactivate();
//        }

//        protected override void OnSaveInstanceState(Bundle outState)
//        {
//            base.OnSaveInstanceState(outState);

//            _mapView.OnSaveInstanceState(outState);
//        }

//        public void OnMapReady(MapboxMap mapbox)
//        {
//            _mapboxMap = mapbox;

//            InitLocationEngine();
//            InitLocationLayer();
//            InitMapRoute();
//            _locationLayer?.OnStart();
//        }

//        private void SetDestination(LatLng point)
//        {
//            _destination = Com.Mapbox.Geojson.Point.FromLngLat(point.Longitude, point.Latitude);
//            _launchRouteButton.Enabled = false;
//            _loadingProgressBar.Visibility = ViewStates.Visible;
//            SetCurrentMarkerPosition(point);
//            if (_currentLocation != null)
//            {
//                FetchRoute();
//            }
//        }

//        public void OnConnected()
//        {
//            //_locationEngine.RequestLocationUpdates();
//            _googleLocationEngine.RequestLocationUpdates();
//        }

//        public void OnLocationChanged(Location location)
//        {
//            _currentLocation = Com.Mapbox.Geojson.Point.FromLngLat(location.Longitude, location.Latitude);
//            OnLocationFound(location);
//        }

//        public void OnFailure(ICall call, Throwable exception)
//        {
//            Console.WriteLine(exception.Message);
//            Snackbar.Make(_mapView, "Failed request", BaseTransientBottomBar.LengthShort).Show();
//            HideLoading();
//        }

//        public void OnResponse(ICall p0, Response response)
//        {
//            HideLoading();

//            if (ValidRouteResponse(response))
//            {
//                _route = ((DirectionsResponse)response.Body()).Routes()[0];

//                if ((double)_route.Distance() > 25d)
//                {
//                    _launchRouteButton.Enabled = true;
//                    _mapRoute.AddRoute(_route);
//                    BoundCameraToRoute();
//                }
//                else
//                {
//                    Snackbar.Make(_mapView, "Please select a longer route", BaseTransientBottomBar.LengthShort).Show();
//                }
//            }
//            else
//            {
//                Snackbar.Make(_mapView, "Can't create route", BaseTransientBottomBar.LengthShort).Show();
//            }

//        }

//        private void InitLocationEngine()
//        {
//            _googleLocationEngine = new GoogleLocationEngine(this);
//            //googleLocationEngine.LastLocation
//            //_locationEngine = new LostLocationEngine(this)
//            //{
//            //    Priority = 3,
//            //    Interval = 0,
//            //    FastestInterval = 1000
//            //};
//            //_locationEngine.AddLocationEngineListener(this);
//            //_locationEngine.Activate();

//            //if (_locationEngine.LastLocation != null)
//            //{
//            //    var lastLocation = _locationEngine.LastLocation;
//            //    OnLocationChanged(lastLocation);
//            //    _currentLocation = Com.Mapbox.Geojson.Point.FromLngLat(lastLocation.Longitude, lastLocation.Latitude);
//            //}

//            _googleLocationEngine = new GoogleLocationEngine(this)
//            {
//                Priority = 3,
//                Interval = 0,
//                FastestInterval = 1000
//            };
//            _googleLocationEngine.AddLocationEngineListener(this);
//            _googleLocationEngine.Activate();

//            if (_googleLocationEngine.LastLocation != null)
//            {
//                var lastLocation = _googleLocationEngine.LastLocation;
//                OnLocationChanged(lastLocation);
//                _currentLocation = Com.Mapbox.Geojson.Point.FromLngLat(lastLocation.Longitude, lastLocation.Latitude);
//            }
//        }

//        private void InitLocationLayer()
//        {
//            //_locationLayer = new LocationLayerPlugin(_mapView, _mapboxMap, _locationEngine);
//            _locationLayer = new LocationLayerPlugin(_mapView, _mapboxMap, _googleLocationEngine);
//            _locationLayer.SetLocationLayerEnabled(LocationLayerMode.Compass);
//        }

//        private void FetchRoute()
//        {
//            NavigationRoute.GetBuilder()
//                .AccessToken("pk.eyJ1IjoiZGVuZWF0aCIsImEiOiJjajh0bWYxenIwcDFuMnJxemE3Y2VpdndmIn0.zIFjC_o9v_rDUxgvZjkkQQ")
//                .Origin(_currentLocation)
//                .Destination(_destination)
//                .Build()
//                .GetRoute(this);

//            _loadingProgressBar.Visibility = ViewStates.Visible;
//        }

//        private void LaunchNavigationWithRoute()
//        {
//            if (_route != null)
//            {
//                var o = NavigationViewOptions.InvokeBuilder().DirectionsRoute(_route).Build();
//                NavigationLauncher.StartNavigation(this, o/*_route, null, false*/);
//            }
//        }

//        private bool ValidRouteResponse(Response response)
//        {
//            return response.Body() != null
//                   && ((DirectionsResponse)response.Body()).Routes() != null
//                   && ((DirectionsResponse)response.Body()).Routes().Count > 0;
//        }

//        private void HideLoading()
//        {
//            if (_loadingProgressBar.Visibility == ViewStates.Visible)
//            {
//                _loadingProgressBar.Visibility = ViewStates.Invisible;
//            }
//        }

//        private void OnLocationFound(Location location)
//        {
//            if (_locationFound)
//                return;

//            _locationFound = true;

//            AnimateCamera(new LatLng(location.Latitude, location.Longitude));
//            HideLoading();
//            SetDestination(new LatLng(_latitude, _longitude));
//        }

//        private void BoundCameraToRoute()
//        {
//            if (_route != null)
//            {
//                var routeCoords = Com.Mapbox.Geojson.LineString.FromPolyline(_route.Geometry(), 6).Coordinates();
//                var bboxPoints = new List<LatLng>();

//                foreach (var point in routeCoords)
//                {
//                    bboxPoints.Add(new LatLng(point.Latitude(), point.Longitude()));
//                }
//                if (bboxPoints.Count > 1)
//                {
//                    try
//                    {
//                        var bounds = new LatLngBounds.Builder().Includes(bboxPoints).Build();
//                        AnimateCameraBbox(bounds, CameraAnimationDuration, new[] { 50, 500, 50, 335 });
//                    }
//                    catch (InvalidLatLngBoundsException)
//                    {
//                        Toast.MakeText(this, "Valid route not found.", ToastLength.Short).Show();
//                    }
//                }
//            }
//        }

//        private void AnimateCameraBbox(LatLngBounds bounds, int animationTime, int[] padding)
//        {
//            _mapboxMap.AnimateCamera(CameraUpdateFactory.NewLatLngBounds(bounds, padding[0], padding[1], padding[2], padding[3]), animationTime);
//        }

//        private void AnimateCamera(LatLng point)
//        {
//            _mapboxMap.AnimateCamera(CameraUpdateFactory.NewLatLngZoom(point, 16), CameraAnimationDuration);
//        }

//        private void InitMapRoute()
//        {
//            _mapRoute = new NavigationMapRoute(_mapView, _mapboxMap);
//        }

//        private void SetCurrentMarkerPosition(LatLng position)
//        {
//            if (position != null)
//            {
//                if (_currentMarker == null)
//                {
//                    var markerViewOptions = new MarkerViewOptions().InvokePosition(position);
//                    _currentMarker = _mapboxMap.AddMarker(markerViewOptions);
//                }
//                else
//                {
//                    _currentMarker.Position = position;
//                }
//            }
//        }
//    }
//}