﻿using System;
using Com.Mapbox.Mapboxsdk.Offline;
using Android.OS;
using Android.Util;
namespace Naxam.Mapbox.Platform.Droid.Offline
{
    public class OfflineRegionObserver: Java.Lang.Object, OfflineRegion.IOfflineRegionObserver
    {
        public OfflineRegionObserver(Action<OfflineRegionStatus> statusHandle, 
                                     Action<OfflineRegionError>  errorHandle,
                                     Action<long> tileCountLimitExceededHanle)
        {
            this.OnStatusChangedHandle = statusHandle;
            this.OnErrorHandle = errorHandle;
            this.OnMapboxTileCountLimitExceededHandle = tileCountLimitExceededHanle;
        }

        internal Action<long> OnMapboxTileCountLimitExceededHandle { get; set; }
        internal Action<OfflineRegionError> OnErrorHandle { get; set; }
        internal Action<OfflineRegionStatus> OnStatusChangedHandle { get; set; }

        public void MapboxTileCountLimitExceeded(long p0)
        {
            OnMapboxTileCountLimitExceededHandle?.Invoke(p0);
        }

        public void OnError(OfflineRegionError p0)
        {
            OnErrorHandle?.Invoke(p0);
        }

        public void OnStatusChanged(OfflineRegionStatus p0)
        {
            try
            {
                OnStatusChangedHandle?.Invoke(p0);
            }
            catch(Exception ex)
            {
                Console.WriteLine($"[OfflineRegionObserver]OnStatusChanged : {ex.Message}");
            }
//System.Diagnostics.
            //this.
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            ConsoleColor tempColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Cyan;
            //System.Diagnostics.Debug.WriteLine("{OfflineRegionObserver}OnStatusChanged");
            Console.WriteLine("{OfflineRegionObserver}Dispose");
            Console.ForegroundColor = tempColor;
            System.Diagnostics.Debug.WriteLine("{OfflineRegionObserver}Dispose");
        }
    }
}
