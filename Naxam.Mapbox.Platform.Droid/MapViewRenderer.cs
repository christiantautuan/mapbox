﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.Support.V7.App;
using Com.Mapbox.Mapboxsdk.Annotations;
using Com.Mapbox.Mapboxsdk.Camera;
using Com.Mapbox.Mapboxsdk.Geometry;
using Com.Mapbox.Mapboxsdk.Maps;
using Java.Util;
using Naxam.Controls.Mapbox.Forms;
using NForms = Naxam.Controls.Mapbox.Forms;
using Naxam.Controls.Mapbox.Platform.Droid;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Annotation = Naxam.Controls.Mapbox.Forms.Annotation;
using Bitmap = Android.Graphics.Bitmap;
using MapView = Naxam.Controls.Mapbox.Forms.MapView;
using Point = Xamarin.Forms.Point;
using Sdk = Com.Mapbox.Mapboxsdk;
using View = Android.Views.View;
using Java.Lang;
using Xamarin.Forms.Xaml;
using Naxam.Mapbox.Platform.Droid;
using Android.Widget;
using System.Threading.Tasks;
using Com.Mapbox.Mapboxsdk.Style.Layers;
using Com.Mapbox.Mapboxsdk.Style.Functions;
using Com.Mapbox.Mapboxsdk.Style.Functions.Stops;
using Com.Mapbox.Mapboxsdk.Constants;
using Android.Graphics.Drawables;
using Android.Locations;
using Com.Mapbox.Mapboxsdk.Style.Sources;

namespace Naxam.Controls.Mapbox.Platform.Droid
{
    public partial class MapViewRenderer
        : ViewRenderer<MapView, View>, MapboxMap.ISnapshotReadyCallback, IOnMapReadyCallback, MapboxMap.IOnMyLocationChangeListener
    {
        MapboxMap map;

        MapViewFragment fragment;
        private const int SIZE_ZOOM = 13;
        private Position currentCamera;

        Dictionary<string, Sdk.Annotations.Annotation> _annotationDictionaries =
                                  new Dictionary<string, Sdk.Annotations.Annotation>();

        public MapViewRenderer(Android.Content.Context context) : base(context)
        {

        }

        protected override void OnElementChanged(
            ElementChangedEventArgs<MapView> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null)
            {
                e.OldElement.TakeSnapshotFunc -= TakeMapSnapshot;
                e.OldElement.GetFeaturesAroundPointFunc -= GetFeaturesAroundPoint;
                if (map != null)
                {
                    RemoveMapEvents();
                }
            }
            //MapboxGeocoding

            if (e.NewElement == null)
                return;

            if (Control == null)
            {
                var activity = (AppCompatActivity)Context;
                var view = new Android.Widget.FrameLayout(activity)
                {
                    Id = GenerateViewId()
                };



                SetNativeControl(view);

                fragment = new MapViewFragment();
                activity.SupportFragmentManager.BeginTransaction()
    .Replace(view.Id, fragment)
    .Commit();


                fragment.GetMapAsync(this);
                currentCamera = new Position();
                //SetUpNavigationEvents();
            }
        }

        public void SetupFunctions()
        {

            Element.TakeSnapshotFunc += TakeMapSnapshot;
            Element.GetFeaturesAroundPointFunc += GetFeaturesAroundPoint;

            Element.ResetPositionAction = () =>
            {
                //TODO handle reset position call
                //map.ResetNorth();
                //map.AnimateCamera(CameraUpdateFactory.ZoomTo(Element.ZoomLevel));
            };
            Element.UpdateLayerFunc = (string layerId, bool isVisible, bool IsCustom) =>
            {
                if (!string.IsNullOrEmpty(layerId))
                {
                    string layerIdStr = IsCustom ? layerId.Prefix() : layerId;
                    var layer = map.GetLayer(layerIdStr);
                    if (layer != null)
                    {
                        layer.SetProperties(layer.Visibility,
                                            isVisible ? Sdk.Style.Layers.PropertyFactory.Visibility(Sdk.Style.Layers.Property.Visible)
                                            : Sdk.Style.Layers.PropertyFactory.Visibility(Sdk.Style.Layers.Property.None));

                        if (IsCustom && Element.MapStyle.CustomLayers != null)
                        {
                            var count = Element.MapStyle.CustomLayers.Count();
                            for (var i = 0; i < count; i++)
                            {
                                if (Element.MapStyle.CustomLayers.ElementAt(i).Id == layerId)
                                {
                                    Element.MapStyle.CustomLayers.ElementAt(i).IsVisible = isVisible;
                                    break;
                                }
                            }
                        }
                        return true;
                    }
                }
                return false;
            };

            Element.UpdateShapeOfSourceFunc = (Annotation annotation, string sourceId) =>
            {
                if (annotation != null && !string.IsNullOrEmpty(sourceId))
                {
                    var shape = annotation.ToFeatureCollection();
                    var source = map.GetSource(sourceId.Prefix()) as Sdk.Style.Sources.GeoJsonSource;
                    if (source != null)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            source.SetGeoJson(shape);
                        });
                        if (Element.MapStyle.CustomSources?.FirstOrDefault((arg) => arg.Id == sourceId) is ShapeSource ss)
                        {
                            ss.Shape = annotation;
                        }
                        //if (Element.MapStyle.CustomSources != null)
                        //{
                        //    var count = Element.MapStyle.CustomSources.Count();
                        //    for (var i = 0; i < count; i++)
                        //    {
                        //        if (Element.MapStyle.CustomSources.ElementAt(i).Id == sourceId)
                        //        {
                        //            Element.MapStyle.CustomSources.ElementAt(i).Shape = annotation;
                        //            break;
                        //        }
                        //    }
                        //}
                        return true;
                    }
                }
                return false;
            };

            Element.ReloadStyleAction = () =>
            {
                //https://github.com/mapbox/mapbox-gl-native/issues/9511
                map.SetStyleUrl(map.StyleUrl, null);
            };

            Element.UpdateViewPortAction = (Position centerLocation, double? zoomLevel, double? bearing, bool animated, Action completionHandler) =>
            {
                var newPosition = new CameraPosition.Builder()
                                                    .Bearing(bearing ?? map.CameraPosition.Bearing)
                                                    .Target(centerLocation?.ToLatLng() ?? map.CameraPosition.Target)
                                                    .Zoom(zoomLevel ?? map.CameraPosition.Zoom)
                                                    .Build();
                var callback = completionHandler == null ? null : new CancelableCallback()
                {
                    FinishHandler = completionHandler,
                    CancelHandler = completionHandler
                };
                var update = CameraUpdateFactory.NewCameraPosition(newPosition);
                if (animated)
                {
                    map.AnimateCamera(update, callback);
                }
                else
                {
                    //map.SetLatLngBoundsForCameraTarget()
                    //map.GetCameraForLatLngBounds()
                    map.MoveCamera(update, callback);
                }

            };

            Element.GetStyleImageFunc += GetStyleImage;
        }

        private byte[] GetStyleImage(string imageName)
        {
            var img = map.GetImage(imageName);
            if (img != null)
            {
                var stream = new MemoryStream();
                img.Compress(Bitmap.CompressFormat.Png, 100, stream);
                return stream.ToArray();
            }
            return null;
        }

        byte[] TakeMapSnapshot()
        {
            //TODO
            map.Snapshot(this);
            return result;
        }

        IFeature[] GetFeaturesAroundPoint(Point point, double radius, string[] layers)
        {
            var output = new List<IFeature>();
            RectF rect = point.ToRect(Context.ToPixels(radius));
            var listFeatures = map.QueryRenderedFeatures(rect, layers);
            return listFeatures.Select(x => x.ToFeature())
                               .Where(x => x != null)
                               .ToArray();
        }

        protected override void Dispose(bool disposing)
        {
            if (fragment != null)
            {
                RemoveMapEvents();

                if (fragment.StateSaved)
                {
                    var activity = (AppCompatActivity)Context;
                    var fm = activity.SupportFragmentManager;

                    fm.BeginTransaction()
                        .Remove(fragment)
                        .Commit();
                }

                fragment.Dispose();
                fragment = null;
            }

            base.Dispose(disposing);
        }

        private Dictionary<string, object> ConvertToDictionary(string featureProperties)
        {
            Dictionary<string, object> objectFeature = JsonConvert.DeserializeObject<Dictionary<string, object>>(featureProperties);
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(objectFeature["properties"].ToString()); ;
        }

        private void FocustoLocation(LatLng latLng)
        {
            if (map == null) { return; }

            CameraPosition position = new CameraPosition.Builder().Target(latLng).Zoom(SIZE_ZOOM).Build();
            ICameraUpdate camera = CameraUpdateFactory.NewCameraPosition(position);
            map.AnimateCamera(camera);
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == MapView.CenterProperty.PropertyName)
            {
                if (!ReferenceEquals(Element.Center, currentCamera))
                {
                    if (Element.Center == null) return;
                    FocustoLocation(Element.Center.ToLatLng());
                }
            }
            else if (e.PropertyName == MapView.MapStyleProperty.PropertyName && map != null)
            {
                UpdateMapStyle();
            }
            else if (e.PropertyName == MapView.PitchEnabledProperty.PropertyName)
            {
                if (map != null)
                {
                    map.UiSettings.TiltGesturesEnabled = Element.PitchEnabled;
                }
            }
            else if (e.PropertyName == MapView.PitchProperty.PropertyName)
            {
                map?.SetTilt(Element.Pitch);
            }
            else if (e.PropertyName == MapView.RotateEnabledProperty.PropertyName)
            {
                if (map != null)
                {
                    map.UiSettings.RotateGesturesEnabled = Element.RotateEnabled;
                }
            }
            else if (e.PropertyName == MapView.RotatedDegreeProperty.PropertyName)
            {
                map?.SetBearing(Element.RotatedDegree);
            }
            else if (e.PropertyName == MapView.AnnotationsProperty.PropertyName)
            {
                RemoveAllAnnotations();
                if (Element.Annotations != null)
                {
                    AddAnnotations(Element.Annotations.ToArray());
                    var notifyCollection = Element.Annotations as INotifyCollectionChanged;
                    if (notifyCollection != null)
                    {
                        notifyCollection.CollectionChanged += OnAnnotationsCollectionChanged;
                    }
                }
            }
            else if (e.PropertyName == MapView.ZoomLevelProperty.PropertyName && map != null)
            {
                var dif = System.Math.Abs(map.CameraPosition.Zoom - Element.ZoomLevel);
                System.Diagnostics.Debug.WriteLine($"Current zoom: {map.CameraPosition.Zoom} - New zoom: {Element.ZoomLevel}");
                if (dif >= 0.01)
                {
                    System.Diagnostics.Debug.WriteLine("Updating zoom level");
                    map.AnimateCamera(CameraUpdateFactory.ZoomTo(Element.ZoomLevel));
                }
            }
        }

        void UpdateMapStyle()
        {
            if (Element.MapStyle != null && !string.IsNullOrEmpty(Element.MapStyle.UrlString))
            {
                map.StyleUrl = Element.MapStyle.UrlString;
                Element.MapStyle.PropertyChanging += OnMapStylePropertyChanging;
                Element.MapStyle.PropertyChanged += OnMapStylePropertyChanged;
            }

        }

        void OnMapStylePropertyChanging(object sender, Xamarin.Forms.PropertyChangingEventArgs e)
        {
            if (e.PropertyName == MapStyle.CustomSourcesProperty.PropertyName
                && (sender as MapStyle).CustomSources != null)
            {
                var notifiyCollection = (sender as MapStyle).CustomSources as INotifyCollectionChanged;
                if (notifiyCollection != null)
                {
                    notifiyCollection.CollectionChanged -= OnShapeSourcesCollectionChanged;
                }
                RemoveSources(Element.MapStyle.CustomSources.ToList());
            }
        }

        void OnMapStylePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var style = sender as MapStyle;
            if (style == null) return;
            if (e.PropertyName == MapStyle.CustomSourcesProperty.PropertyName
                && style.CustomSources != null)
            {
                var notifiyCollection = style.CustomSources as INotifyCollectionChanged;
                if (notifiyCollection != null)
                {
                    notifiyCollection.CollectionChanged += OnShapeSourcesCollectionChanged;
                }

                AddSources(style.CustomSources.ToList());
            }
            else if (e.PropertyName == MapStyle.CustomLayersProperty.PropertyName
                     && style.CustomLayers != null)
            {
                var notifiyCollection = Element.MapStyle.CustomLayers as INotifyCollectionChanged;
                if (notifiyCollection != null)
                {
                    notifiyCollection.CollectionChanged += OnLayersCollectionChanged;
                }

                AddLayers(Element.MapStyle.CustomLayers.ToList());
            }
        }

        void OnShapeSourcesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                AddSources(e.NewItems.Cast<MapSource>().ToList());
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                RemoveSources(e.OldItems.Cast<MapSource>().ToList());
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                var sources = map.Sources;
                foreach (var s in sources)
                {
                    if (s.Id.HasPrefix())
                    {
                        map.RemoveSource(s);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                RemoveSources(e.OldItems.Cast<MapSource>().ToList());
                AddSources(e.NewItems.Cast<MapSource>().ToList());
            }
        }

        void AddSources(List<MapSource> sources)
        {
            if (sources == null || map == null)
            {
                return;
            }

            foreach (MapSource ms in sources)
            {
                if (ms.Id != null)
                {
                    if (ms is ShapeSource ss && ss.Shape != null)
                    {
                        var shape = ss.Shape.ToFeatureCollection();

                        var source = map.GetSource(ss.Id.Prefix()) as Sdk.Style.Sources.GeoJsonSource;

                        if (source == null)
                        {
                            source = new Sdk.Style.Sources.GeoJsonSource(ss.Id.Prefix(), shape);
                            map.AddSource(source);
                        }
                        else
                        {
                            source.SetGeoJson(shape);
                        }
                    }
                    else
                    {
                        //TODO handle RasterSource
                    }
                }
            }
        }

        void RemoveSources(List<MapSource> sources)
        {
            if (sources == null)
            {
                return;
            }
            foreach (MapSource source in sources)
            {
                if (source.Id != null)
                {
                    map.RemoveSource(source.Id.Prefix());
                }
            }
        }

        void OnLayersCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                AddLayers(e.NewItems.Cast<NForms.Layer>().ToList());
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                RemoveLayers(e.OldItems);
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                var layers = map.Layers;
                foreach (var layer in layers)
                {
                    if (layer.Id.HasPrefix())
                    {
                        map.RemoveLayer(layer);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                RemoveLayers(e.OldItems);

                AddLayers(e.NewItems.Cast<NForms.Layer>().ToList());
            }
        }

        void RemoveLayers(System.Collections.IList layers)
        {
            if (layers == null)
            {
                return;
            }
            foreach (NForms.Layer layer in layers)
            {
                var native = map.GetLayer(layer.Id.Prefix());

                if (native != null)
                {
                    map.RemoveLayer(native);
                }
            }
        }

        void AddLayers(List<Naxam.Controls.Mapbox.Forms.Layer> layers)
        {
            if (layers == null)
            {
                return;
            }
            foreach (NForms.Layer layer in layers)
            {
                if (string.IsNullOrEmpty(layer.Id))
                {
                    continue;
                }

                map.RemoveLayer(layer.Id.Prefix());

                if (layer is NForms.CircleLayer)
                {
                    var cross = (NForms.CircleLayer)layer;

                    var source = map.GetSource(cross.SourceId.Prefix());
                    if (source == null)
                    {
                        continue;
                    }

                    map.AddLayer(cross.ToNative());
                }
                else if (layer is NForms.LineLayer)
                {
                    var cross = (NForms.LineLayer)layer;

                    var source = map.GetSource(cross.SourceId.Prefix());
                    if (source == null)
                    {
                        continue;
                    }

                    map.AddLayer(cross.ToNative());
                }
            }
        }

        private void Annot_PropertyChanging(object sender, Xamarin.Forms.PropertyChangingEventArgs e)
        {
            //if (e.PropertyName.Equals(PointAnnotation.CoordinateProperty.PropertyName))
            //UpdateAnnotation(sender as Annotation);
        }

        void Annot_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(PointAnnotation.CoordinateProperty.PropertyName))
                UpdateAnnotation(sender as Annotation);
        }


        async void UpdateAnnotation(Annotation annotation)
        {
            //var index = map.Markers.ToList().FindIndex((obj) =>
            //{
            //    if (obj is AnnotationMarkerOptions annot)
            //        if (string.IsNullOrEmpty(obj.Id) == false)
            //            return annot.Id.Equals(annotation.Id);
            //    return false;
            //});
            if (_annotationDictionaries.ContainsKey(annotation.Id))
            {
                if (annotation is PointAnnotation pointAnnotation)
                {
                    if (pointAnnotation.Icon == typeof(Stream))
                    {
                        var marker = map.Markers.ToList().Find((Marker obj) => obj.Id == _annotationDictionaries[annotation.Id].Id);
                        marker.Position = pointAnnotation.Coordinate.ToLatLng();
                        var markerView = new MarkerViewOptions();
                        markerView.InvokeTitle(pointAnnotation.Title);
                        markerView.InvokeSnippet(pointAnnotation.SubTitle);
                        markerView.InvokePosition(pointAnnotation.Coordinate.ToLatLng());
                        markerView.Anchor((float)pointAnnotation.Anchor.X, (float)pointAnnotation.Anchor.Y);
                        markerView.InvokeRotation((float)pointAnnotation.Rotation);
                        markerView.Flat(pointAnnotation.IsFlat);
                        markerView.Marker.Id = marker.Id;
                        //marker.
                        //marker
                        //markerView.mar
                        //if (marker != null)
                        //{
                        //    map.UpdateMarker(marker);
                        //    //marker.
                        //}
                        map.RemoveAnnotation(_annotationDictionaries[annotation.Id]);
                        //_annotationDictionaries.Remove(annotation.Id);
                        await AddAnnotation(annotation);
                    }
                    else
                    {
                        map.RemoveAnnotation(_annotationDictionaries[annotation.Id]);
                        //_annotationDictionaries.Remove(annotation.Id);
                        await AddAnnotation(annotation);
                    }
                }
                else
                {
                    //MapView.annotations
                    //(_annotationDictionaries[annotation.Id].set
                    map.RemoveAnnotation(_annotationDictionaries[annotation.Id]);
                    //_annotationDictionaries.Remove(annotation.Id);
                    await AddAnnotation(annotation);
                    //map.Markers[index].Position = pointAnnotation.Coordinate.ToLatLng();
                    //map.Annotations[index]
                    //(map.Annotations[index] as PointAnnotation).Coordinate = pointAnnotation.Coordinate.ToCLCoordinate();
                    //var imglAnnot = MapView.Annotations[index] as MGLImageAnnotation;
                    //MapView.RemoveAnnotation(imglAnnot);
                    //imglAnnot.Coordinate = pointAnnotation.Coordinate.ToCLCoordinate();
                    //MapView.AddAnnotation(imglAnnot);
                    //}
                }
            }
        }

        private async void OnAnnotationsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Annotation annot in e.NewItems)
                {
                    annot.PropertyChanging += Annot_PropertyChanging;
                    annot.PropertyChanged += Annot_PropertyChanged;

                    await AddAnnotation(annot);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                var items = new List<Annotation>();
                foreach (Annotation annot in e.OldItems)
                {
                    annot.PropertyChanging -= Annot_PropertyChanging;
                    annot.PropertyChanged -= Annot_PropertyChanged;
                    items.Add(annot);
                }
                RemoveAnnotations(items.ToArray());
                foreach (var item in items)
                {
                    _annotationDictionaries.Remove(item.Id);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                map.RemoveAnnotations();
                _annotationDictionaries.Clear();
            }
            else if (e.Action == NotifyCollectionChangedAction.Replace)
            {
                var itemsToRemove = new List<Annotation>();
                foreach (Annotation annot in e.OldItems)
                {
                    annot.PropertyChanging -= Annot_PropertyChanging;
                    annot.PropertyChanged -= Annot_PropertyChanged;
                    itemsToRemove.Add(annot);
                }
                RemoveAnnotations(itemsToRemove.ToArray());


                var itemsToAdd = new List<Annotation>();
                foreach (Annotation annot in e.NewItems)
                {
                    annot.PropertyChanging += Annot_PropertyChanging;
                    annot.PropertyChanged += Annot_PropertyChanged;
                    itemsToRemove.Add(annot);
                }
                AddAnnotations(itemsToAdd.ToArray());
            }
        }

        void RemoveAnnotations(Annotation[] annotations)
        {
            var currentAnnotations = map.Annotations;
            if (currentAnnotations == null)
            {
                return;
            }
            var annots = new List<Sdk.Annotations.Annotation>();
            foreach (Annotation at in annotations)
            {
                if (_annotationDictionaries.ContainsKey(at.Id))
                {
                    annots.Add(_annotationDictionaries[at.Id]);
                }
            }
            map.RemoveAnnotations(annots.ToArray());
        }

        async void AddAnnotations(Annotation[] annotations)
        {
            foreach (Annotation at in annotations)
            {
                await AddAnnotation(at);
            }
        }

        public async Task<Bitmap> CreateBitmapFromView(Xamarin.Forms.View view)
        {
            //BitmapUtils.
            var iconView = view;
            var nativeView = await BitmapUtils.ConvertFormsToNative(iconView, new Rectangle(0, 0, (double)BitmapUtils.DpToPx((float)iconView.WidthRequest), (double)BitmapUtils.DpToPx((float)iconView.HeightRequest)), Xamarin.Forms.Platform.Android.Platform.CreateRendererWithContext(iconView, Context));
            var otherView = new FrameLayout(nativeView.Context);
            nativeView.LayoutParameters = new FrameLayout.LayoutParams(BitmapUtils.DpToPx((float)iconView.WidthRequest), BitmapUtils.DpToPx((float)iconView.HeightRequest));
            otherView.AddView(nativeView);
            //nativeItem.SetIcon(await Utils.ConvertViewToBitmapDescriptor(otherView));
            //nativeItem.SetAnchor((float)outerItem.Anchor.X, (float)outerItem.Anchor.Y);
            //nativeItem.Visible = true;
            //BitmapUtils.ConvertViewToBitmap(nativeView);
            return BitmapUtils.ConvertViewToBitmap(nativeView); ;
        }

        private async Task<Sdk.Annotations.Annotation> AddAnnotation(Annotation at)
        {

            System.Diagnostics.Debug.WriteLine($"Adding Annotation  Type:{at.GetType()} | Id : {at.Id}");
            Sdk.Annotations.Annotation options = null;
            if (at is PointAnnotation)
            {
                Bitmap imageB = null;
                var pointAnnotation = (PointAnnotation)at;
                var markerView = new MarkerViewOptions();
                markerView.InvokeTitle(at.Title);
                markerView.InvokeSnippet(at.SubTitle);
                markerView.InvokePosition(pointAnnotation.Coordinate.ToLatLng());
                markerView.Anchor((float)pointAnnotation.Anchor.X, (float)pointAnnotation.Anchor.Y);
                markerView.InvokeRotation((float)pointAnnotation.Rotation);
                markerView.Flat(pointAnnotation.IsFlat);
                //markerView.flat
                if (pointAnnotation.Icon != null)
                {
                    if (pointAnnotation.Icon == typeof(string))
                    {
                        int resourceId = Resources.GetIdentifier(pointAnnotation.Icon, "drawable", Context.PackageName);
                        imageB = await Resources.GetBitmapAsync(pointAnnotation.Icon);
                        //map.anchor
                        if (IconFactory.GetInstance(Context).FromResource(resourceId) != null)
                        {                           
                            //drawable = Context.GetDrawable(resourceId);
                            var bitmap = ((BitmapDrawable)Context.GetDrawable(resourceId)).Bitmap;

                            if (pointAnnotation.Icon.Width != null & pointAnnotation.Icon.Height != null)
                                markerView.InvokeIcon(IconFactory.GetInstance(Context).FromBitmap(Bitmap.CreateScaledBitmap(bitmap, BitmapUtils.DpToPx((float)pointAnnotation.Icon.Width), BitmapUtils.DpToPx((float)pointAnnotation.Icon.Height), false)));
                            else
                                markerView.InvokeIcon(IconFactory.GetInstance(Context).FromBitmap(bitmap));
                            //markerView.InvokeIcon(IconFactory.GetInstance(Context).FromResource(resourceId));
                            //marker.SetIcon(IconFactory.GetInstance(Context).FromResource(resourceId));
                        }
                    }
                    else if (pointAnnotation.Icon == typeof(Xamarin.Forms.View))
                    {
                        // PropertyFactory.iconSize(
                        //  Function.property(
                        //    "selected",
                        //    Stops.categorical(
                        //      Stop.stop(true, PropertyFactory.iconSize(1.5f)),
                        //      Stop.stop(false, PropertyFactory.iconSize(1.0f))
                        //    )
                        //  )
                        //)
                        var AndroidView = BitmapUtils.ConvertFormsToNative2(pointAnnotation.Icon, new Rectangle(0, 0, BitmapUtils.DpToPx((float)((Xamarin.Forms.View)pointAnnotation.Icon).WidthRequest), BitmapUtils.DpToPx((float)((Xamarin.Forms.View)pointAnnotation.Icon).HeightRequest)));
                        var bitmap = BitmapUtils.ConvertViewToBitMap2(AndroidView);
                        imageB = BitmapUtils.ConvertViewToBitMap2(AndroidView);
                        //marker.SetIcon(IconFactory.GetInstance(Context).FromBitmap(bitmap));
                        if (pointAnnotation.Icon.Width != null & pointAnnotation.Icon.Height != null)
                            markerView.InvokeIcon(IconFactory.GetInstance(Context).FromBitmap(Bitmap.CreateScaledBitmap(bitmap, BitmapUtils.DpToPx((float)pointAnnotation.Icon.Width),BitmapUtils.DpToPx((float)pointAnnotation.Icon.Height), true)));
                        else
                            markerView.InvokeIcon(IconFactory.GetInstance(Context).FromBitmap(bitmap));
                        //markerView.InvokeIcon(IconFactory.GetInstance(Context).FromBitmap(bitmap));
                    }
                    else if (pointAnnotation.Icon == typeof(Stream))
                    {
                        ((MemoryStream)pointAnnotation.Icon).Position = 0;
                        //((MemoryStream)pointAnnotation.Icon).SetLength(0);
                        //((MemoryStream)pointAnnotation.Icon).Seek(0, SeekOrigin.Begin);
                        var bitmapFactory = await BitmapFactory.DecodeStreamAsync(pointAnnotation.Icon);
                        imageB = BitmapFactory.DecodeStream(pointAnnotation.Icon);
                        MemoryStream asd = new MemoryStream();
                        //asd.Capacity
                        //asd.Seek(0, SeekOrigin.Begin);
                        //imageStream.Close();
                        //imageStream.re
                        if (pointAnnotation.Icon.Width != null & pointAnnotation.Icon.Height != null)
                            markerView.InvokeIcon(IconFactory.GetInstance(Context).FromBitmap(Bitmap.CreateScaledBitmap(bitmapFactory,BitmapUtils.DpToPx((float)pointAnnotation.Icon.Width), BitmapUtils.DpToPx((float)pointAnnotation.Icon.Height), true)));
                        else
                            markerView.InvokeIcon(IconFactory.GetInstance(Context).FromBitmap(bitmapFactory));
                    }
                }

                //marker.Marker.SetTopOffsetPixels((int)pointAnnotation.Anchor.Y*3);
                //marker.Marker.SetRightOffsetPixels((int)pointAnnotation.Anchor.X*3);
                //markerView.
                //new Sdk.Style.Layers.SymbolLayer()
                //Sdk.Style.Layers.SymbolLayer myLayer = new Sdk.Style.Layers.SymbolLayer(1);
                //map.AddLayer(myLayer);
                //if (imageB != null)
                //{
                //    //myLayer.P
                //    map.AddImage($"{pointAnnotation.Id}.image", imageB);
                //    myLayer.WithProperties(PropertyFactory.IconImage($"{pointAnnotation.Id}.image"),PropertyFactory.IconAnchor("center"),PropertyFactory.IconOffset( new Float[]{ new Float(0.5), new Float(.5) }));
                //    //myLayer.icon
                //    //myLayer.WithProperties()
                //    map.AddLayer(myLayer);
                //}
                options = map.AddMarker(markerView);
                //options = map.AddMarker(marker);
            }
            else if (at is PolylineAnnotation)
            {
                var polyline = at as PolylineAnnotation;
                if (polyline.Coordinates?.Count() == 0)
                {
                    return null;
                }
                var coordins = new ArrayList();
                for (var i = 0; i < polyline.Coordinates.Count(); i++)
                {
                    coordins.Add(polyline.Coordinates.ElementAt(i).ToLatLng());
                }
                if (_annotationDictionaries.ContainsKey(at.Id))
                {
                    map.RemovePolyline(_annotationDictionaries[at.Id] as Polyline);
                    _annotationDictionaries.Remove(at.Id);
                    //var polylineOpts = new PolylineOptions();
                    //Context.
                    //polylineOpts.Polyline.Width = (float)polyline.LineWidth;
                    //polylineOpts.Polyline.Color = polyline.LineColor.ToAndroid();
                    //polylineOpts.AddAll(coordins);
                    //options = map.AddPolyline(polylineOpts);
                    //_annotationDictionaries.Add(at.Id, options);
                }
               
                var polylineOpts = new PolylineOptions();
                polylineOpts.Polyline.Width = (float)polyline.LineWidth;
                polylineOpts.Polyline.Color = polyline.LineColor.ToAndroid();
                polylineOpts.AddAll(coordins);
                options = map.AddPolyline(polylineOpts);
                _annotationDictionaries.Add(at.Id, options);

                //
                var notifyCollection = polyline.Coordinates as INotifyCollectionChanged;

                if (notifyCollection != null)
                {
                    notifyCollection.CollectionChanged += (s, e) =>
                    {
                        if (e.Action == NotifyCollectionChangedAction.Add)
                        {
                            if (_annotationDictionaries.ContainsKey(at.Id))
                            {
                                var poly = _annotationDictionaries[at.Id] as Polyline;
                                poly.AddPoint(polyline.Coordinates.ElementAt(e.NewStartingIndex).ToLatLng());
                            }
                            else
                            {
                                var coords = new ArrayList();
                                for (var i = 0; i < polyline.Coordinates.Count(); i++)
                                {
                                    coords.Add(polyline.Coordinates.ElementAt(i).ToLatLng());
                                }
                                var polylineOpt = new PolylineOptions();
                                polylineOpt.Polyline.Width = (float)polyline.LineWidth;
                                polylineOpt.Polyline.Color = polyline.LineColor.ToAndroid();
                                polylineOpt.AddAll(coords);
                                options = map.AddPolyline(polylineOpt);
                                _annotationDictionaries.Add(at.Id, options);
                            }
                        }
                        else if (e.Action == NotifyCollectionChangedAction.Remove)
                        {
                            if (_annotationDictionaries.ContainsKey(at.Id))
                            {
                                var poly = _annotationDictionaries[at.Id] as Polyline;
                                poly.Points.Remove(polyline.Coordinates.ElementAt(e.OldStartingIndex).ToLatLng());
                            }
                        }
                    };
                }
            }
            else if (at is MultiPolylineAnnotation)
            {
                var polyline = at as MultiPolylineAnnotation;
                if (polyline.Coordinates == null || polyline.Coordinates.Length == 0)
                {
                    return null;
                }

                var lines = new List<PolylineOptions>();
                for (var i = 0; i < polyline.Coordinates.Length; i++)
                {
                    if (polyline.Coordinates[i].Length == 0)
                    {
                        continue;
                    }
                    var coords = new PolylineOptions();
                    for (var j = 0; j < polyline.Coordinates[i].Length; j++)
                    {
                        coords.Add(new LatLng(polyline.Coordinates[i][j].Lat, polyline.Coordinates[i][j].Long));
                    }
                    lines.Add(coords);
                }
                map.AddPolylines(lines);
            }
            if (options != null)
            {
                if (at.Id != null)
                {
                    if (_annotationDictionaries.ContainsKey(at.Id))
                    {
                        _annotationDictionaries[at.Id] = options;
                    }
                    else
                    {
                        _annotationDictionaries.Add(at.Id, options);
                    }
                }
            }

            return options;
        }

        void RemoveAllAnnotations()
        {
            if (map != null)
                if (map.Annotations != null)
                {
                    map.RemoveAnnotations(map.Annotations);
                }
        }

        private byte[] result;
        void MapboxMap.ISnapshotReadyCallback.OnSnapshotReady(Bitmap bmp)
        {
            MemoryStream stream = new MemoryStream();
            bmp.Compress(Bitmap.CompressFormat.Png, 0, stream);
            result = stream.ToArray();
        }

        void Map_SetOnMyLocationChangeListener(MapboxMap.IOnMyLocationChangeListener obj)
        {
        }


        public void OnMapReady(MapboxMap p0)
        {
            map = p0;
            //map.MyLocationEnabled = true;
            map.UiSettings.RotateGesturesEnabled = Element.RotateEnabled;
            map.UiSettings.TiltGesturesEnabled = Element.PitchEnabled;
            map.SetOnMyLocationChangeListener(this);
            map.MyLocationEnabled = Element.ShowUserLocation;
            map.TrackingSettings.MyLocationTrackingMode = Element.ShowUserLocation ? MyLocationTracking.TrackingFollow : MyLocationTracking.TrackingNone;
            //map.MyLocationViewSettings.SetBackgroundDrawable(Drawable.)
            //int resourceId = Resources.GetIdentifier(pointAnnotation.Icon, "drawable", Context.PackageName);
            //map.MyLocationViewSettings.SetBackgroundDrawable()
            //map.
            //if (Element.UserIcon != null)
            //{
            //    Drawable drawable = default(Drawable);
            //    if (Element.UserIcon == typeof(string))
            //    {
            //        int resourceId = Resources.GetIdentifier("car", "drawable", Context.PackageName);
            //        drawable = Context.GetDrawable(resourceId);
            //    }
            //    else if (Element.UserIcon == typeof(Xamarin.Forms.View))
            //    {
            //        Xamarin.Forms.View formsView = (Xamarin.Forms.View)Element.UserIcon;
            //        var nativeView = BitmapUtils.ConvertFormsToNative2(formsView, new Rectangle(0, 0, formsView.WidthRequest, formsView.HeightRequest));
            //        drawable = new BitmapDrawable(BitmapUtils.ConvertViewToBitMap2(nativeView));
            //    }

            //    //map.MyLocationViewSettings.SetBackgroundDrawable(Context.GetDrawable(resourceId), new int[] { resourceId }); //change user location pin icon
            //    map.MyLocationViewSettings.SetForegroundDrawable(drawable, drawable); //change user location pin icon
            //}
            if (Element.UserPinLocation != null)
            {
                if (Element.UserPinLocation.Icon != null)
                {
                    Drawable drawable = default(Drawable);
                    if (Element.UserPinLocation.Icon == typeof(string))
                    {
                        int resourceId = Resources.GetIdentifier((string)Element.UserPinLocation.Icon, "drawable", Context.PackageName);
                        //drawable = Context.GetDrawable(resourceId);
                        var bitmapDrawable = ((BitmapDrawable)Context.GetDrawable(resourceId));
                        if(Element.UserPinLocation.Icon.Width != null & Element.UserPinLocation.Icon.Height != null)
                            drawable = new BitmapDrawable(Context.Resources, Bitmap.CreateScaledBitmap(bitmapDrawable.Bitmap,BitmapUtils.DpToPx((float)Element.UserPinLocation.Icon.Width),BitmapUtils.DpToPx((float)Element.UserPinLocation.Icon.Height),true));
                        else
                            drawable = Context.GetDrawable(resourceId);
                    }
                    else if (Element.UserPinLocation.Icon == typeof(Xamarin.Forms.View))
                    {
                        Xamarin.Forms.View formsView = (Xamarin.Forms.View)Element.UserPinLocation.Icon;
                        var nativeView = BitmapUtils.ConvertFormsToNative2(formsView, new Rectangle(0, 0, Element.UserPinLocation.Icon.Width == null ? formsView.WidthRequest : (double)Element.UserPinLocation.Icon.Width, Element.UserPinLocation.Icon.Height == null ? formsView.HeightRequest : (double)Element.UserPinLocation.Icon.Height));
                        var bitmap = BitmapUtils.ConvertViewToBitMap2(nativeView);
                        //Bitmap.CreateBitmap(bitmap,0,0,)
                        drawable = new BitmapDrawable(BitmapUtils.ConvertViewToBitMap2(nativeView));
                    }
                    else if (Element.UserPinLocation.Icon == typeof(Stream))
                    {
                        var bitmap = BitmapFactory.DecodeStream(Element.UserPinLocation.Icon);
                        if (Element.UserPinLocation.Icon.Width != null & Element.UserPinLocation.Icon.Height != null)
                            drawable = new BitmapDrawable(Context.Resources, Bitmap.CreateScaledBitmap(bitmap, BitmapUtils.DpToPx((float)Element.UserPinLocation.Icon.Width), BitmapUtils.DpToPx((float)Element.UserPinLocation.Icon.Height), true));
                        else
                            drawable = new BitmapDrawable(bitmap);
                    }

                    //map.MyLocationViewSettings.SetBackgroundDrawable(Context.GetDrawable(resourceId), new int[] { resourceId }); //change user location pin icont
                    try
                    {
                        map.MyLocationViewSettings.SetForegroundDrawable(drawable, drawable); //change user location pin icon
                    }
                    catch(System.Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine($"Message: {ex.Message}, StackTrace: {ex.StackTrace}");
                    }
                }
            }

            if (Element.Center != null)
            {
                map.CameraPosition = new CameraPosition.Builder()
                    .Target(Element.Center.ToLatLng())
               .Zoom(Element.ZoomLevel)
                    .Tilt(Element.Pitch)
                    .Bearing(Element.RotatedDegree)
               .Build();
            }
            else
            {
                map.CameraPosition = new CameraPosition.Builder()
                    .Target(map.CameraPosition.Target)
                    .Zoom(Element.ZoomLevel)
                    .Tilt(Element.Pitch)
                    .Bearing(Element.RotatedDegree)
                    .Build();
            }

            AddMapEvents();
            SetupFunctions();
            if (Element.MapStyle == null)
            {
                if (map.StyleUrl != null)
                {
                    Element.MapStyle = new MapStyle(map.StyleUrl);
                }
            }
            else
            {
                UpdateMapStyle();
            }
        }

        public void OnMyLocationChange(Location p0)
        {
            Element.Log("my Location changed");
        }
    }
}
