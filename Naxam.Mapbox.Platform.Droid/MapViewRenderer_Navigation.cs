﻿//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.ComponentModel;
//using System.IO;
//using System.Linq;
//using Android.Content;
//using Android.Graphics;
//using Android.Support.V7.App;
//using Com.Mapbox.Mapboxsdk.Annotations;
//using Com.Mapbox.Mapboxsdk.Camera;
//using Com.Mapbox.Mapboxsdk.Geometry;
//using Com.Mapbox.Mapboxsdk.Maps;
//using Java.Util;
//using Naxam.Controls.Mapbox.Forms;
//using Naxam.Controls.Mapbox.Platform.Droid;
//using Newtonsoft.Json;
//using Xamarin.Forms;
//using Xamarin.Forms.Platform.Android;
//using Annotation = Naxam.Controls.Mapbox.Forms.Annotation;
//using Bitmap = Android.Graphics.Bitmap;
//using NCMapView = Naxam.Controls.Mapbox.Forms.MapView;
//using MapView = Com.Mapbox.Mapboxsdk.Maps.MapView;
//using Point = Xamarin.Forms.Point;
//using Sdk = Com.Mapbox.Mapboxsdk;
//using View = Android.Views.View;
//using Java.Lang;
//using Xamarin.Forms.Xaml;
//using Naxam.Mapbox.Platform.Droid;
//using Android.Widget;
//using System.Threading.Tasks;
//using Com.Mapbox.Services.Android.Navigation.UI.V5.Route;
//using GoogleGson;
//using Square.Retrofit2;
//using Com.Mapbox.Geojson;
//using Com.Mapbox.Mapboxsdk;
//using Android.Bluetooth.LE;
//using Android.Support.Design.Widget;
//using Android;
////using Com.Mapbox.Api.Directions.V5.Models;
//using Com.Mapbox.Services.Android.Navigation.V5.Navigation;
//using Com.Mapbox.Api.Directions.V5.Models;
//using Com.Mapbox.Services.Android.Navigation.V5.Milestone;
//using MTrigger = Com.Mapbox.Services.Android.Navigation.V5.Milestone.Trigger;
//using Com.Mapbox.Api.Directions.V5;
//using Com.Mapbox.Services.Android.Navigation.V5.Instruction;
//using Com.Mapbox.Services.Android.Navigation.V5.Routeprogress;
//using Android.Locations;
//using Com.Mapbox.Services.Api.Utils.Turf;
//using Com.Mapbox.Api.Optimization.V1;
//using GPoint = Com.Mapbox.Geojson.Point;
//using SCGPoint = Com.Mapbox.Services.Commons.Geojson.Point;
//using Com.Mapbox.Services.Android.Telemetry.Location;
//using Com.Mapbox.Services.Android.Location;
//using Android.Runtime;

//namespace Naxam.Controls.Mapbox.Platform.Droid
//{
//    public partial class MapViewRenderer : ICallback, IOnRouteSelectionChangeListener
//    {
//        MapboxNavigation navigation;
//        NavigationMapRoute navigationMapRoute;
//        public int BEGIN_ROUTE_MILESTONE =13456;

//        void SetUpNavigationEvents()
//        {
//            navigation = new MapboxNavigation(Context, NCMapView.AccessToken);

//            navigation.AddMilestone(new RouteMilestone.Builder()
//              .SetIdentifier(BEGIN_ROUTE_MILESTONE)
//              .SetInstruction(new BeginRouteInstruction())
//              .SetTrigger(
//                        MTrigger.All(
//                        MTrigger.Lt(TriggerProperty.StepIndex, 3),
//                        MTrigger.Gt(TriggerProperty.StepDistanceTotalMeters, 200),
//                        MTrigger.Gte(TriggerProperty.StepDistanceTraveledMeters, 75)
//                    )
//              ).Build());
//            //Location
//            Com.Mapbox.Services.Android.Telemetry.Location.LocationEngine location = new LocationEngineProvider(Context).ObtainBestLocationEngineAvailable();
//            navigation.LocationEngine = location;
//            navigation.NavigationEvent += Navigation_NavigationEvent;
//            //navigationMapRoute = new NavigationMapRoute(null, map, mapboxMap, "admin-3-4-boundaries-bg");
//            //Gson gson = new GsonBuilder().RegisterTypeAdapterFactory(DirectionsAdapterFactory.Create()).Create();
//            ////var json = loadJsonFromAsset(DIRECTIONS_RESPONSE);// TODO Load Directions
//            //DirectionsResponse response = (DirectionsResponse)gson.FromJson(json, Class.FromType(typeof(DirectionsResponse)));

//            //navigationMapRoute.AddRoute(response.Routes()[0]);

//            Element.ToShowNavigation += Element_ToShowNavigation;

//        }


//        //override dispose
//        //override OnDestroy()
//        //{
            
//        //}

//        void Element_ToShowNavigation(Position start, Position end)
//        {

//            var route = GetNavigationRoute(GPoint.FromLngLat(start.Lat, start.Long), GPoint.FromLngLat(end.Lat, end.Long));
//            if (navigation != null && route != null)
//            {

//                // Hide the start button
//                //startRouteButton.Visibility = (ViewStates.Invisible);

//                // Attach all of our navigation listeners.
//                //navigation.AddNavigationEventListener(this);
//                //navigation.AddProgressChangeListener(this);
//                //navigation.AddMilestoneEventListener(this);
//                //navigation.AddOffRouteListener(this);

//                Com.Mapbox.Services.Android.Telemetry.Location.LocationEngine location = new LocationEngineProvider(Context).ObtainBestLocationEngineAvailable();
//                //location
//                navigation.LocationEngine = location;
//                navigation.NavigationEvent += Navigation_NavigationEvent;
//                navigation.ProgressChange += Navigation_ProgressChange;
//                navigation.MilestoneEvent += Navigation_MilestoneEvent;
//                navigation.OffRoute += Navigation_OffRoute;
//                //var mleLocation = ((MockLocationEngine)location);
//                //mleLocation.SetRoute();
//                 //((MockLocationEngine)location).SetRoute(route.Result.Routes()[0]);
//                navigation.LocationEngine = (location);
//                navigation.StartNavigation(route.Result.Routes()[0]);
//                //mapboxMap.SetOnMapClickListener(null);
//            }

//        }


//        void Navigation_NavigationEvent(object sender, NavigationEventEventArgs e)
//        {
//            if (e.P0)
//            {
//                System.Diagnostics.Debug.WriteLine("Running");
//            }
//        }

//        void Navigation_ProgressChange(object sender, ProgressChangeEventArgs e)
//        {
//        }

//        void Navigation_MilestoneEvent(object sender, MilestoneEventEventArgs e)
//        {
//        }

//        void Navigation_OffRoute(object sender, Com.Mapbox.Services.Android.Navigation.V5.Offroute.OffRouteEventArgs e)
//        {
//        }


//        public void OnNewPrimaryRouteSelected(DirectionsRoute p0)
//        {
//            //throw new NotImplementedException();
//        }


//         async Task<DirectionsResponse> GetNavigationRoute(GPoint start, GPoint end)
//        {
//           var response = await NavigationRoute.GetBuilder()
//                           .AccessToken(Element.GetAccessToken())
//                           .Origin(start)
//                           .Destination(end)
//                           .Build()
//                           .GetRouteAsync();
//            return response;
//        }

//        //public void RequestDirectionsRoute(Com.Mapbox.Geojson.Point origin, Com.Mapbox.Geojson.Point destination)
//        //{
//        //    MapboxDirections directions = MapboxDirections
//        //        .InvokeBuilder()
//        //        .Origin(origin)
//        //        .Destination(destination)
//        //        .AccessToken(Element.GetAccessToken())
//        //        .Profile(DirectionsCriteria.ProfileDrivingTraffic)
//        //        .Overview(DirectionsCriteria.OverviewFull)
//        //        .Annotations(DirectionsCriteria.AnnotationCongestion)
//        //        .Alternatives(new Java.Lang.Boolean(true))
//        //        .Steps(new Java.Lang.Boolean(true))
//        //        .Build();

//        //    directions.EnqueueCall(this);
//        //}


//        public void OnFailure(ICall p0, Throwable p1)
//        {
//            System.Diagnostics.Debug.WriteLine(p1);
//        }

//        public void OnResponse(ICall call, Response response)
//        {
//            var directions = Android.Runtime.Extensions.JavaCast<DirectionsResponse>(response.Body());

//            if (directions != null && directions.Routes().Count > 0)
//            {
//                //this.routes = directions.Routes();
//                //navigationMapRoute.AddRoutes(routes);
//            }
//        }

//        class BeginRouteInstruction : Instruction
//        {
//            public override string BuildInstruction(RouteProgress p0)
//            {
//                return "Have a safe trip!";
//            }
//        }

//        void CalculateRoute(GPoint endPoint)
//        {
//            Location userLocation = map.MyLocation;
//            if (userLocation == null)
//            {
//                System.Diagnostics.Debug.WriteLine("calculateRoute: User location is null, therefore, origin can't be set.");
//                return;
//            }
//            //Com.Mapbox.Core.po

//            GPoint origin = GPoint.FromLngLat(userLocation.Longitude, userLocation.Latitude);
//            //if (TurfMeasurement.Distance(origin, endPoint, TurfConstants.UnitMeters) < 50)
//            //{
//            //    //startRouteButton.Visibility = (ViewStates.Gone);
//            //    return;
//            //}
//            var tripStops = new List<GPoint>();

//            tripStops.Add(origin);
//            tripStops.Add(endPoint);

//            var builder = MapboxOptimization.InvokeBuilder();
//            builder.AccessToken(Element.GetAccessToken());
//            builder.Coordinates(tripStops);

//            var client = builder.Build();

//            client.EnableDebug(true);
//            client.EnqueueCall(this);
//        }

//        //public void OnNewPrimaryRouteSelected(DirectionsRoute p0)
//        //{
//        //    throw new NotImplementedException();
//        //}
//    }
//    public class StyleCycle
//    {
//        private static readonly string[] STYLES = {
//                        Com.Mapbox.Mapboxsdk.Constants.Style.MapboxStreets,
//                        Com.Mapbox.Mapboxsdk.Constants.Style.Outdoors,
//                        Com.Mapbox.Mapboxsdk.Constants.Style.Light,
//                        Com.Mapbox.Mapboxsdk.Constants.Style.Dark,
//                        Com.Mapbox.Mapboxsdk.Constants.Style.SatelliteStreets
//                    };
//        private int index;

//        public string GetNextStyle()
//        {
//            //Com.Mapbox.Mapboxsdk.Constants.Style.IStyleUrl
//            index++;
//            if (index == STYLES.Length)
//            {
//                index = 0;
//            }
//            return GetStyle();
//        }

//        public string GetStyle()
//        {
//            return STYLES[index];
//        }
//    }
//}
