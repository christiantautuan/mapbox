﻿using System;
using System.Collections.Specialized;
using System.Linq;
using Com.Mapbox.Mapboxsdk.Maps;
using Naxam.Controls.Mapbox.Forms;
using MapView = Com.Mapbox.Mapboxsdk.Maps.MapView;
using Com.Mapbox.Mapboxsdk.Annotations;
using Xamarin.Forms.Internals;
using Naxam.Mapbox.Forms;
using Com.Mapbox.Mapboxsdk.Geometry;
using System.Drawing;
using System.Collections.Generic;
using Xamarin.Forms;
using Com.Mapbox.Mapboxsdk.Camera;
using Naxam.Mapbox.Forms.Models;

namespace Naxam.Controls.Mapbox.Platform.Droid
{

    public partial class MapViewRenderer : MapView.IOnMapChangedListener ,IMoveCamera
    {
        //protected MapViewRenderer()
        //{
        //}

        void AddMapEvents ()
        {
            map.MarkerClick += MarkerClicked;
            map.MapClick += MapClicked;
            map.MyLocationChange += MyLocationChanged;
            map.CameraIdle += OnCameraIdle;
            map.CameraMove += Map_CameraMove;
            fragment.OnMapChangedListener = (this);
            map.CameraMoveStarted += Map_CameraMoveStarted;
            Element.moveCamera += Element_MoveCamera;
            Element.MapboxDelegate = this;
            Element.movetToRegion += Element_MovetToRegion;
            //map.CameraMoveStarted
        }
 
        void RemoveMapEvents ()
        {
            map.MarkerClick -= MarkerClicked;
            map.MapClick -= MapClicked;
            map.MyLocationChange -= MyLocationChanged;
            map.CameraIdle -= OnCameraIdle;
            map.CameraMoveStarted -= Map_CameraMoveStarted;

            fragment.OnMapChangedListener = null;
        }

        void Element_MoveCamera(CoordinateBounds arg1, Thickness arg2, bool isAnimate)
        {
            //visibleRegionCommand = new Command<MapSpan>((MapSpan mapSpan) => {
            //        map.MoveToRegion(mapSpan);
            //    });
            //    fitMapToPositionsCommand = new Command<List<Position>>((List<Position> positions) => {
            //        map.MoveCamera(CameraUpdateFactory.NewBounds(Bounds.FromPositions(positions), 100));

            var builder = new LatLngBounds.Builder();
            builder.Include(arg1.NorthEast.ToLatLng());
            builder.Include(arg1.SouthWest.ToLatLng());

                    //map.MoveCamera(          map.SetVisibleCoordinateBounds(new MGLCoordinateBounds() { ne = arg1.NorthEast.ToCLCoordinate(), sw = arg1.SouthWest.ToCLCoordinate() }, new UIEdgeInsets((nfloat)arg2.Top, (nfloat)arg2.Left, (nfloat)arg2.Bottom, (nfloat)arg2.Right), isAnimate);
            map.MoveCamera(CameraUpdateFactory.NewLatLngBounds(builder.Build(), (int)arg2.Top, (int)arg2.Left, (int)arg2.Bottom, (int)arg2.Right));
        }

        void Element_MovetToRegion(CameraSpan arg1)
        {
            //MapView.SetCamera(new MGLMapCamera()
            //{
            //    CenterCoordinate = arg1.Center.ToCLCoordinate(),
            //    Altitude = arg1.Altitude,
            //    Heading = arg1.Heading,
            //    Pitch = (nfloat)arg1.Pitch
            //}, true);
            //map.setca

            //MapView.()
        }

        void Map_CameraMoveStarted(object sender, MapboxMap.CameraMoveStartedEventArgs e)
        {
            //map.SetLatLngBoundsForCameraTarget(LatLngBounds.From(1, 2, 3, 4));
            //map.CameraPosition.
            //System.Diagnostics.Debug.WriteLine($"Map_CameraMoveStarted(lat,lang):({map.CameraPosition.Target.Latitude},{map.CameraPosition.Target.Longitude})");
            Element.SetDidDragStartedEvent(new CameraPositionEventArg()
            {
                PositionCamera = new Naxam.Mapbox.Forms.CameraPosition()
                {
                    Target = new Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude),
                    Bearing = map.CameraPosition.Bearing,
                    Tilt = map.CameraPosition.Tilt,
                    Zoom = map.CameraPosition.Zoom,
                    //BoundPadding = 
                },
                P0 = e.P0
            }); 
            Element.DidDragStartedCommand?.Execute( new CameraPositionEventArg()
            {
                PositionCamera = new Naxam.Mapbox.Forms.CameraPosition()
                {
                    Target = new Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude),
                    Bearing = map.CameraPosition.Bearing,
                    Tilt = map.CameraPosition.Tilt,
                    Zoom = map.CameraPosition.Zoom,
                    //BoundPadding = 
                },
                P0 = e.P0
            }); 
        }

		private void OnCameraIdle(object sender, EventArgs e)
		{
            currentCamera.Lat = map.CameraPosition.Target.Latitude;
            currentCamera.Long = map.CameraPosition.Target.Longitude;
			Element.ZoomLevel = map.CameraPosition.Zoom;
            Element.Center = currentCamera;
            //Element.SetDidDragFinishedEvent($"Finished(lat,lang):({map.CameraPosition.Target.Latitude},{map.CameraPosition.Target.Longitude})"); 
            //System.Diagnostics.Debug.WriteLine($"Map_CameraMoveStarted(lat,lang):({map.CameraPosition.Target.Latitude},{map.CameraPosition.Target.Longitude})");
            Element.SetDidDragFinishedEvent(new CameraPositionEventArg()
            {
                PositionCamera = new Naxam.Mapbox.Forms.CameraPosition()
                {
                    Target = new Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude),
                    Bearing = map.CameraPosition.Bearing,
                    Tilt = map.CameraPosition.Tilt,
                    Zoom = map.CameraPosition.Zoom,
                    //BoundPadding = 
                }
            }); 
            Element.DidDragFinishedEventCommand?.Execute(new CameraPositionEventArg()
            {
                PositionCamera = new Naxam.Mapbox.Forms.CameraPosition()
                {
                    Target = new Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude),
                    Bearing = map.CameraPosition.Bearing,
                    Tilt = map.CameraPosition.Tilt,
                    Zoom = map.CameraPosition.Zoom,
                    //BoundPadding = 
                }
            });
		}

        void Map_CameraMove(object sender, EventArgs e)
        {
            //Element.SetDidDragEvent($"CameraMove(lat,lang):({map.CameraPosition.Target.Latitude},{map.CameraPosition.Target.Longitude})"); 
            //System.Diagnostics.Debug.WriteLine($"Map_CameraMoveStarted(lat,lang):({map.CameraPosition.Target.Latitude},{map.CameraPosition.Target.Longitude})");
            //map.Projection.VisibleRegion.LatLngBounds.
            //new Bounds(NativeMap.Projection.VisibleRegion.NearLeft.ToPosition(), NativeMap.Projection.VisibleRegion.FarRight.ToPosition());
            var bounds = map.Projection.VisibleRegion.LatLngBounds;
            Element.CameraBounds = new Bounds(bounds.NorthEast.ToPosition(), bounds.NorthWest.ToPosition(), bounds.SouthEast.ToPosition(), bounds.SouthWest.ToPosition());
            Element.SetDidDragEvent(new CameraPositionEventArg()
            {
                PositionCamera = new Naxam.Mapbox.Forms.CameraPosition()
                {
                    Target = new Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude),
                    Bearing = map.CameraPosition.Bearing,
                    Tilt = map.CameraPosition.Tilt,
                    Zoom = map.CameraPosition.Zoom,
                    //BoundPadding = 
                }
            }); 
            Element.DidDragEventCommand?.Execute(new CameraPositionEventArg()
            {
                PositionCamera = new Naxam.Mapbox.Forms.CameraPosition()
                {
                    Target = new Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude),
                    Bearing = map.CameraPosition.Bearing,
                    Tilt = map.CameraPosition.Tilt,
                    Zoom = map.CameraPosition.Zoom,
                    //BoundPadding = 
                }
            });
        }


        void MyLocationChanged(object o, MapboxMap.MyLocationChangeEventArgs args)
        {
            if (Element.UserLocation == null)
                Element.UserLocation = new Position();

            Element.UserLocation.Lat = args.P0.Latitude;
            Element.UserLocation.Long = args.P0.Longitude;
            Element.SetUserLocationChangedEvent(new UserLocationEventArgs(args.P0.Latitude, args.P0.Longitude));
            //Element.(new UserLocationEventArgs(args.P0.Latitude, args.P0.Longitude));
            Element.UserLocationChangedCommand?.Execute(new UserLocationEventArgs(args.P0.Latitude, args.P0.Longitude));
        }

        void MapClicked (object o, MapboxMap.MapClickEventArgs args)
        {
            Element.FocusPosition = false;
            bool isWater = false;
            var point = map.Projection.ToScreenLocation (args.P0);
            var xfPoint = new Xamarin.Forms.Point (point.X, point.Y);
            var xfPosition = new Position (args.P0.Latitude, args.P0.Longitude);

            // CUSTOMIZING PIN TODO
            //map.AddMarker(new MarkerViewOptions().InvokeIcon())
            //map.AddMarker (new MarkerViewOptions ()
            // .SetTitle("Test Marker")
            // .SetPosition(new LatLng(41.885, -87.679))
            // .SetAnchor(0.5f, 1.0f) // this should be bottom center
             //.SetIcon(IconFactory.GetInstance(this).FromResource(Resource.Drawable.Driver_Pin))
            //);

            //LatLng center = map.CameraPosition.Target;
            //var pixel = map.Projection.ToScreenLocation(center);
            var isWaterPointResult = map.QueryRenderedFeatures(point, "water");
            isWater = isWaterPointResult.Any();

            Element.DidTapOnMapCommand?.Execute (new Pin(xfPosition, isWater));
            //Element.SetDidTapOnMapEvent(xfPosition);
            Element.SetDidTapOnMapEvent(new Pin(xfPosition, isWater));
        }

        void MarkerClicked (object o, MapboxMap.MarkerClickEventArgs args)
        {
            
            Element.Center.Lat = args.P0.Position.Latitude;
            Element.Center.Long = args.P0.Position.Longitude;
            Element.IsMarkerClicked = true;
            //args.
            var annotationKey = _annotationDictionaries.FirstOrDefault (x => x.Value == args.P0).Key;
            System.Diagnostics.Debug.WriteLine($"Key: {annotationKey}");
            if (Element.CanShowCalloutChecker?.Invoke (annotationKey) == true) {
                if (Element.CanViewPinInfo)
                    args.P0.ShowInfoWindow (map, fragment.View as MapView);
            }
            Element.SetPinClickedEvent(new PinClickedEventArgs(new PointAnnotation()
            {
                Id = args.P0.Id.ToString(),
                Coordinate = new Position(args.P0.Position.Latitude, args.P0.Position.Longitude)
            }));

            Element.PinClickedCommand?.Execute(new PinClickedEventArgs(new PointAnnotation()
            {
                Id = args.P0.Id.ToString(),
                Coordinate = new Position(args.P0.Position.Latitude, args.P0.Position.Longitude)
            }));

        }

        public void OnMapChanged (int p0)
        {
            switch (p0) {
            case MapView.DidFinishLoadingStyle:
                var mapStyle = Element.MapStyle;
                if (mapStyle == null
                    || (!string.IsNullOrEmpty (map.StyleUrl) && mapStyle.UrlString != map.StyleUrl)) {
                    mapStyle = new MapStyle (map.StyleUrl);
                   
                }
                    if (mapStyle.CustomSources != null)
					{
						var notifiyCollection = Element.MapStyle.CustomSources as INotifyCollectionChanged;
						if (notifiyCollection != null)
						{
							notifiyCollection.CollectionChanged += OnShapeSourcesCollectionChanged;
						}

						AddSources(Element.MapStyle.CustomSources.ToList());
					}
                    if (mapStyle.CustomLayers != null)
					{
						if (Element.MapStyle.CustomLayers is INotifyCollectionChanged notifiyCollection)
						{
							notifiyCollection.CollectionChanged += OnLayersCollectionChanged;
						}

						AddLayers(Element.MapStyle.CustomLayers.ToList());
					}
                    mapStyle.OriginalLayers = map.Layers.Select((arg) =>
                                                                        new Layer(arg.Id) 
                                                                       ).ToArray();
					Element.MapStyle = mapStyle;
                    Element.DidFinishLoadingStyleCommand?.Execute(mapStyle);
                    Element.SetDidFinishLoadingStyleEvent(mapStyle);
                break;
            case MapView.DidFinishRenderingMap:
					Element.Center = new Position(map.CameraPosition.Target.Latitude, map.CameraPosition.Target.Longitude);
                Element.DidFinishRenderingCommand?.Execute (false);
                    Element.SetDidFinishRenderingEvent(false);
                break;
            case MapView.DidFinishRenderingMapFullyRendered:
                Element.DidFinishRenderingCommand?.Execute (true);
                    Element.SetDidFinishRenderingEvent(true);
                break;
            case MapView.RegionDidChange:
					Element.RegionDidChangeCommand?.Execute (false);
                    Element.SetRegionDidChangeEvent(false);
                break;
            case MapView.RegionDidChangeAnimated:
                Element.RegionDidChangeCommand?.Execute (true);
                Element.SetRegionDidChangeEvent(true);
                break;
            default:
                break;
            }
        }

        public void MoveCamera(CoordinateBounds bounds, Thickness padding, bool isAnimate = true)
        {
            var builder = new LatLngBounds.Builder();
            builder.Include(bounds.NorthEast.ToLatLng());
            builder.Include(bounds.SouthWest.ToLatLng());
            var moveCamera = CameraUpdateFactory.NewLatLngBounds(builder.Build(), (int)padding.Top, (int)padding.Left, (int)padding.Bottom, (int)padding.Right);
            if (isAnimate)
            {
                map.AnimateCamera(moveCamera);
            }
            else
            {
                map.MoveCamera(moveCamera);
            }

        }

        public void MoveCamera(List<Position> bounds, Thickness padding, bool isAnimate = true)
        {
            var builder = new LatLngBounds.Builder();
            foreach(Position pos in bounds)
            {
                builder.Include(pos.ToLatLng());
            }

            var moveCamera = CameraUpdateFactory.NewLatLngBounds(builder.Build(),(int)padding.Left, (int)padding.Top, (int)padding.Right ,  (int)padding.Bottom);
            if (isAnimate)
            {
                map.AnimateCamera(moveCamera);
            }
            else
            {
                map.MoveCamera(moveCamera);
            }
        }
    }
}