download offline

public IOfflineStorageService offlineService;

//init
offlineService = DependencyService.Get<IOfflineStorageService>();
// progress changed
offlineService.OfflinePackProgressChanged += (sender, e) =>
{
    //System.Diagnostics.Debug.WriteLine($"asdasdasd==============>>>>>>>>>>>{1}");
	var progress = e.OfflinePack.Progress;
    //System.Diagnostics.Debug.WriteLine($"e");
    //App.Log($"{e.OfflinePack.Progress}")
	float percentage = 0;
	if (progress.CountOfResourcesExpected > 0)
	{
		percentage = (float)progress.CountOfResourcesCompleted / progress.CountOfResourcesExpected;
	}
    App.Log($"Downloaded Bytes: {progress.CountOfBytesCompleted} ({percentage * 100} %)");
	App.Log($"Downloaded resources: {progress.CountOfResourcesCompleted} ({percentage * 100} %)");
    App.Log($"Downloaded tiles byte: {progress.CountOfTileBytesCompleted}");
    App.Log($"Downloaded tiles: {progress.CountOfTilesCompleted}");
	if (progress.CountOfResourcesExpected == progress.CountOfResourcesCompleted)
	{
		System.Diagnostics.Debug.WriteLine("Download completed");
		Device.BeginInvokeOnMainThread(() =>
		{
			UserDialogs.Instance.HideLoading();
		});
	}

};

//download map bound
if (viewModel.offlineService != null)
{
    var region = new OfflinePackRegion()
    {
        StyleURL = viewModel.CurrentMapStyle.UrlString,
        MaximumZoomLevel = 16,
        MinimumZoomLevel = 9,
        Bounds = new CoordinateBounds()
        {
            NorthEast = Map.CameraBounds.NorthEast,
            SouthWest = Map.CameraBounds.SouthWest
        }
    };
    UserDialogs.Instance.ShowLoading();
    var pack = await viewModel.offlineService.DownloadMap(region, new System.Collections.Generic.Dictionary<string, string>() {
        {"name", "test"},
        {"started_at", DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy")}
    });
    if (pack != null)
    {
        viewModel.offlineService.RequestPackProgress(pack);
    }
    else
    {
        UserDialogs.Instance.HideLoading();
    }
}

// clear download packs
private async void ClearOfflinePacks(object obj)
{
	var packs = await offlineService.GetPacks();
	if (packs != null)
	{
		foreach (OfflinePack pack in packs)
		{
			await offlineService.RemovePack(pack);
		}
	}
}

//load downloaded packs
var packs = await offlineService.GetPacks();
if (packs != null && packs.Length != 0)
{
	var buttons = new List<string>();
	foreach (OfflinePack pack in packs)
	{
		if (pack.Info != null
			&& pack.Info.TryGetValue("name", out string name)
			&& pack.Info.TryGetValue("started_at", out string startTime))
		{
			buttons.Add(name + " - " + startTime);
		}
	}
	var chosen = await UserDialogs.Instance.ActionSheetAsync("Load offline pack", "Cancel", null, null, buttons.ToArray());
	var chosenIndex = buttons.IndexOf(chosen);
	if (chosenIndex >= 0 && chosenIndex < packs.Length)
	{
		var chosenPack = packs[chosenIndex];
        if (chosenPack.Region != null)
        {

            forcedRegion = chosenPack.Region;
            CurrentMapStyle = new MapStyle(chosenPack.Region.StyleURL);
        }
	}
}
else
{
	await UserDialogs.Instance.AlertAsync("There's no offline pack to load");
}
