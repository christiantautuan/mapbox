﻿using System;
using System.IO;
using System.Reflection;

namespace MapBoxQs.Renderer
{
    public static class StatiClass
    {
        //public StatiClass()
        //{
        //}

        public static Stream getRawStremFromEmbeddedResource(this string fileName, double width, double height)
        {
            byte[] buffer = null;
            var assembly = typeof(StatiClass).GetTypeInfo().Assembly;
            using (var stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.Resources.{fileName}"))
            {
                if (stream == null)
                    throw new FileNotFoundException("File not found, make sure that the file extension is included.", fileName);
                buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int)stream.Length);
                using (var editableImage = Plugin.ImageEdit.CrossImageEdit.Current.CreateImage(buffer))
                {
                    var modified = editableImage.Resize(((int)(width * App.nativeScale)), ((int)(height * App.nativeScale))).ToPng();
                    return new MemoryStream(modified);
                }
            }
        }

    }


}
