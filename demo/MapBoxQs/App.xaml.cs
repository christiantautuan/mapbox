﻿using System;
using Xamarin.Forms;
using Naxam.Controls.Mapbox.Forms;


[assembly:Xamarin.Forms.Xaml.XamlCompilation(Xamarin.Forms.Xaml.XamlCompilationOptions.Compile)]
namespace MapBoxQs
{
    public partial class App : Application
    {
        public static double nativeScale { get; set; }
        //public static double nativeScale { get; set; }
        public static double screenWidth { get; set; }
        public static double screenHeight { get; set; }
        public static double screenScale
        {
            get { return (screenWidth + screenHeight) / (320.0f + 568.0f); } //Every measure is based on iOS 320 scale
        }

        public App()
        {
            InitializeComponent();

            MapView.AccessToken = "sk.eyJ1IjoidGhlLXNlZW5lci05OTkiLCJhIjoiY2prajNvdGZrMTdhMjN2b2dkNHRkZHY2eSJ9.uXx3TrZ3CNUkkN2JyDamFQ";
            MainPage = new NavigationPage(new MapBoxQsPage());
            //MainPage = new MapNavigationPage();

        }

        public static void Log(string msg, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            msg = DateTime.Now.ToString("HH:mm:ss tt") + " [PFL]-[" + memberName + "]: " + msg;
            //LogEvent?.Invoke(null, msg);
#if DEBUG
            System.Diagnostics.Debug.WriteLine(msg);
#else
            DependencyService.Get<ILogServices>().Log(msg);
#endif
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
