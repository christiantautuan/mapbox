﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Naxam.Controls.Mapbox.Forms;
using System.Collections.ObjectModel;
using Naxam.Mapbox.Forms;
using System;
using System.Linq;
using Newtonsoft.Json;
using Acr.UserDialogs;
using MapBoxQs.Renderer;
//using Xamarin.Forms.PlatformConfiguration.TizenSpecific;

namespace MapBoxQs
{
    public partial class MapBoxQsPage : ContentPage
    {
        bool isSearchPositionAddress = false, isPinDriver;
        MapBoxQs.Services.IMapBoxService MBServices = new MapBoxQs.Services.MapBoxService();
        MainPageViewModel viewModel = new MainPageViewModel();
        ObservableCollection<Position> polylines;
        public MapBoxQsPage()
        {
            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<iOS>().SetUseSafeArea(true);
            BindingContext = viewModel;
            InitializeComponent();
            Map.UserPinLocation = new UserPin()
            {
                Id = "userlocation",
                Rotation = 90,
                Icon = PinIconFactory.SetIcon("icon",40,40),
            };
            Map.DidDragEvent += Map_DidDragEvent;
            Map.DidDragFinished += Map_DidDragFinished;
            Map.DidDragStarted += Map_DidDragStarted;
            Map.PinClicked += Map_PinClicked;
            Map.DidTapOnMap += Map_DidTapOnMap;
            Map.DidFinishRendering += Map_DidFinishRendering;
            //Map.region
            //viewModel.Pins.Add(new PolylineAnnotation()
            //{
            //    Coordinates = polylines,
            //});
            //Map.InsertLayerAboveLayerFunc
            //Map.tak
            //viewModel.CenterLocation = new Position(10.337510, 123.930414);
            //Map.ShowUserLocation = false;
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            //viewModel.CenterLocation = new Position(10.337510, 123.930414);
            //Map.Center = new Position(10.337510, 123.930414);
        }

        void Map_DidFinishRendering(object sender, bool e)
        {
            //Map.FlowDirection.
            if (e)
            {
                //Map.ShowUserLocation = false;
                //Map.ShowUserLocation = true;
                Map.Center = new Position(10.337510, 123.930414);
                if (polylines == null)
                {

                    polylines = new ObservableCollection<Position>() { new Position(0, 0) };
                    if (viewModel.Pins == null)
                    {
                        viewModel.Pins = new ObservableCollection<Annotation>();
                    }
                    viewModel.Pins.Add(new PolylineAnnotation()
                    {
                        Id = "100",
                        Coordinates = polylines,
                    });
                }

                Map.MapStyle = DefaultMapStyle.MapboxStreets;
            }
        }


        void OnMyLocationButton_Clicked(object sender, System.EventArgs e)
        {
            //viewModel.CenterLocation = new Position(10.337510, 123.930414);
            //Map.UserLocation
            Map.Center = new Position(10.337510, 123.930414);
            //viewModel.chan

        }

        void Map_PinClicked(object sender, PinClickedEventArgs e)
        {

            //DisplayAlert("asd", PlaceMode.Driving_Traffic.ToString(), "Okay");
            //Map.Bounds
            App.Log($"{JsonConvert.SerializeObject(e)}");
        }

        void OnSearchViaPosition_Clicked(object sender, System.EventArgs e)
        {
            isSearchPositionAddress = !isSearchPositionAddress;
            if (isSearchPositionAddress)
            {
                (sender as View).BackgroundColor = Color.Red;
            }
            else
            {
                (sender as View).BackgroundColor = Color.Transparent;
            }
        }

        int counter = 1;
        async void Map_DidTapOnMap(object sender, Pin e)
        {
            if (e.isWater == false)
            {
                if (isSearchPositionAddress == false)
                {
                    var startPoint = new PointAnnotation()
                    {
                        Id = isPinDriver ? "101" : "102",
                        Coordinate = e.position,
                        Title = "Testing Title",
                        SubTitle = "Testing SubTitle",
                        Anchor = new Point(.5D, isPinDriver ? 1D : 1.5D),
                        Rotation = 0,
                        IsDraggable = false
                    };
                    if (isPinDriver)
                    {
                        startPoint.IsFlat = true;
                        //startPoint.Icon = PinIconFactory.SetIcon(Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon");
                        startPoint.Icon = PinIconFactory.SetIcon("bluePin.png".getRawStremFromEmbeddedResource(28, 45),28 , 45);
                    }
                    else
                    {
                        var layout = new Grid() { HeightRequest = 40, WidthRequest = 40, BackgroundColor = Color.Transparent };
                        var button = new Button() { CornerRadius = 20, Image = Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon", BackgroundColor = Color.Red, HorizontalOptions = LayoutOptions.Fill, VerticalOptions = LayoutOptions.Fill, Margin = new Thickness(5) };
                        //var image = new Image() { Source = Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon",  Margin = new Thickness(20) , Aspect = Aspect.Fill };
                        var label = new Label() { TextColor = Color.Red, FontAttributes = FontAttributes.Bold, FontSize = 10, Text = "TEST", HorizontalOptions = LayoutOptions.Fill, VerticalOptions = LayoutOptions.Fill, VerticalTextAlignment = TextAlignment.Center, HorizontalTextAlignment = TextAlignment.Center };

                        layout.Children.Add(button);
                        //layout.Children.Add(image);
                        layout.Children.Add(label);
                        startPoint.Icon = PinIconFactory.SetIcon(Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon",40,40);
                    }

                    if (viewModel.Pins == null)
                    {
                        viewModel.Pins = new ObservableCollection<Annotation>(new Annotation[] { startPoint });
                    }
                    else
                    {
                        if (viewModel.Pins.ToList().FindIndex((obj) => obj.Id.Equals(startPoint.Id)) != -1)
                        {

                            (viewModel.Pins[viewModel.Pins.ToList().FindIndex((obj) => obj.Id.Equals(startPoint.Id))] as PointAnnotation).Rotation += isPinDriver ? 5 : 10;
                            (viewModel.Pins[viewModel.Pins.ToList().FindIndex((obj) => obj.Id.Equals(startPoint.Id))] as PointAnnotation).Coordinate = e.position;
                        }
                        else
                        {
                            viewModel.Pins.Add(startPoint);
                        }
                    }
                }
                else
                {

                    var result = await MBServices.SearchPlaceFromPosition(e.position);
                    OnSearchButton_Clicked(null, null);
                    searchResultListView.ItemsSource = result.features;
                }
            }
        }


        private void Map_DidDragFinished(object sender, CameraPositionEventArg e)
        {
            App.Log($"Position(lat,long): ({e.PositionCamera.Target.Lat},{e.PositionCamera.Target.Long})");
            App.Log($"Tilt: {e.PositionCamera.Tilt} ");
            App.Log($"Zoom: {e.PositionCamera.Zoom} ");
            App.Log($"Bearing: {e.PositionCamera.Bearing}");
            //if(viewModel != null)
                //if(viewModel.Pins != null)
                    //if(viewModel.Pins.Any())
                        //if(viewModel.Pins.ToList().FindIndex((obj) => obj.Id.Equals(isPinDriver ? "101" : "102")) != -1)
                             //viewModel.Pins[viewModel.Pins.ToList().FindIndex((obj) => obj.Id.Equals(isPinDriver ? "101" : "102"))].Rotation -= 180;
        }

        void Map_DidDragStarted(object sender, CameraPositionEventArg e)
        {
            App.Log($"Position(lat,long): ({e.PositionCamera.Target.Lat},{e.PositionCamera.Target.Long})");
            App.Log($"Tilt: {e.PositionCamera.Tilt} ");
            App.Log($"Zoom: {e.PositionCamera.Zoom} ");
            App.Log($"Bearing: {e.PositionCamera.Bearing}");
            App.Log($"P0: {e.P0}");
            //if (viewModel != null)
                //if (viewModel.Pins != null)
                    //if (viewModel.Pins.Any())
                        //if (viewModel.Pins.ToList().FindIndex((obj) => obj.Id.Equals(isPinDriver ? "101" : "102")) != -1)
                            //viewModel.Pins[viewModel.Pins.ToList().FindIndex((obj) => obj.Id.Equals(isPinDriver ? "101" : "102"))].Rotation += 180;
            //Map.RotatedDegree
        }


        private void Map_DidDragEvent(object sender, CameraPositionEventArg e)
        {
            //Map.reload
        }

        void OnAddPolylineButton_Clicked(object sender, System.EventArgs e)
        {
            if (polylines == null)
            {
                polylines = new ObservableCollection<Position>();
                if (viewModel.Pins == null)
                {
                    viewModel.Pins = new ObservableCollection<Annotation>();
                }
                viewModel.Pins.Add(new PolylineAnnotation()
                {
                    Id = "100",
                    Coordinates = polylines,
                });
            }
            //polylines = new ObservableCollection<Position>();
            polylines.Add(new Position(viewModel.CenterLocation.Lat, viewModel.CenterLocation.Long));
            polylines.Add(new Position(viewModel.CenterLocation.Lat + .001D, viewModel.CenterLocation.Long + .001D));
            polylines.Add(new Position(viewModel.CenterLocation.Lat + .002D, viewModel.CenterLocation.Long + .002D));
            polylines.Add(new Position(viewModel.CenterLocation.Lat + .003D, viewModel.CenterLocation.Long + .003D));
            //viewModel.Pins.Add(new PolylineAnnotation()
            //{
            //    Coordinates = polylines,
            //});
        }

        void OnSearchButton_Clicked(object sender, System.EventArgs e)
        {
            searchPlacesView.IsVisible = !searchPlacesView.IsVisible;
        }

        async void OnDirectionButton_Clicked(object sender, System.EventArgs e)
        {
            //MBServices
            //var direction = await MBServices.GetDirection(new Position(10.337510, 123.930414), new Position(10.351897, 123.946114));
            ////if (viewModel.Pins == null)
            ////{
            ////    viewModel.Pins = new ObservableCollection<Annotation>();
            ////}

            //var directionPolylines = new ObservableCollection<Position>() { direction.routes[0].geometry.ways[0] };
            //direction.routes[0].geometry.ways.RemoveAt(0);
            //if (viewModel.Pins == null)
            //{
            //    viewModel.Pins = new ObservableCollection<Annotation>();
            //}
            //viewModel.Pins.Add(new PolylineAnnotation()
            //{
            //    Id = "100",
            //    Coordinates = directionPolylines,
            //});

            //foreach (var position in direction.routes[0].geometry.ways)
            //{
            //    directionPolylines.Add(position);
            //}

            var direction = await MBServices.GetDirection(new Position(10.337510, 123.930414), new Position(10.351897, 123.946114));
            if (viewModel.Pins == null)
            {
                viewModel.Pins = new ObservableCollection<Annotation>();
            }
            var directionPolylines = new ObservableCollection<Position>() { direction.routes[0].geometry.ways[0] };
            direction.routes[0].geometry.ways.RemoveAt(0);
            if (viewModel.Pins == null)
            {
                viewModel.Pins = new ObservableCollection<Annotation>();
            }
            viewModel.Pins.Add(new PolylineAnnotation()
            {
                Id = "200",
                Coordinates = direction.routes[0].geometry.ways,
                //LineColor = Color.Aqua,
                //LineWidth = 10
            });

            foreach (var position in direction.routes[0].geometry.ways)
            {
                directionPolylines.Add(position);
            }

        }

        async void OnSearchText_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (e.NewTextValue.Length >= 3)
            {
                var result = await MBServices.SearchPlace(e.NewTextValue);
                searchResultListView.ItemsSource = result.features;
            }
        }

        void OnShowNavigationButton_Clicked(object sender, System.EventArgs e)
        {
            //Map.ShowNavigation(new Position(10.337510, 123.930414), new Position(10.351897, 123.946114));
            isPinDriver = !isPinDriver;
            (sender as View).BackgroundColor = isPinDriver == true ? Color.Red : Color.Blue;
            (sender as Button).Text = isPinDriver ? "Is Image" : "Is View";

        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            if (viewModel.offlineService != null)
            {
                var region = new OfflinePackRegion()
                {
                    StyleURL = viewModel.CurrentMapStyle.UrlString,
                    MaximumZoomLevel = 16,
                    MinimumZoomLevel = 9,
                    Bounds = new CoordinateBounds()
                    {
                        NorthEast = Map.CameraBounds.NorthEast,
                        SouthWest = Map.CameraBounds.SouthWest
                    }
                };
                //(new CoordinateBounds()).
                UserDialogs.Instance.ShowLoading();
                var pack = await viewModel.offlineService.DownloadMap(region, new System.Collections.Generic.Dictionary<string, string>() {
                    {"name", "test"},
                    {"started_at", DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy")}
                });
                if (pack != null)
                {
                    viewModel.offlineService.RequestPackProgress(pack);
                }
                else
                {
                    UserDialogs.Instance.HideLoading();
                }
            }
        }

        void MoveCamera_Clicked(object sender, System.EventArgs e)
        {
            var startPoint = new PointAnnotation()
            {
                Id = "1234",
                Coordinate = new Position(10.337510, 123.930414),
                Title = "Testing Title",
                SubTitle = "Testing SubTitle",
                Icon = PinIconFactory.SetIcon(Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon")
            };
            viewModel.Pins.Add(startPoint);

            var endPoint = new PointAnnotation()
            {
                Id = "1234",
                Coordinate = new Position(10.321897, 123.976114),
                Title = "Testing Title",
                SubTitle = "Testing SubTitle",
                Icon = PinIconFactory.SetIcon(Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon")
            };
            viewModel.Pins.Add(endPoint);

            var midPoint = new PointAnnotation()
            {
                Id = "1234",
                Coordinate = new Position(10.37927048, 123.96560766),
                Title = "Pulpogan",
                SubTitle = "Pulpogan Street ... .. ...",
                //Icon = PinIconFactory.SetIcon(new Button(){BackgroundColor = Color.Blue, WidthRequest = 20 , HeightRequest = 20})
                Icon = PinIconFactory.SetIcon("bluePin.png".getRawStremFromEmbeddedResource(28, 45),28, 45)
            };
            viewModel.Pins.Add(midPoint);

            //Map.MoveCamera(new CoordinateBounds(new Position(10.337510, 123.930414), new Position(10.321897, 123.976114)), new Thickness(25, 25, 25, 25), true);
            Map.MoveCamera(new List<Position>() { new Position(10.337510, 123.930414), new Position(10.321897, 123.976114) ,new Position(10.37927048, 123.96560766)}, new Thickness(50, 50, 50, 50), true);
            //new Position(10.337510, 123.930414), new Position(10.321897, 123.976114)
            //Map.MoveToRegion(new CameraSpan(new Position(10.337510, 123.930414), 90, 100);
        }

        void MoveCamera_Clicked2(object sender, System.EventArgs e)
        {
            //var startPoint = new PointAnnotation()
            //{
            //    Id = "1234",
            //    Coordinate = new Position(10.357510, 123.910414),
            //    Title = "Testing Title",
            //    SubTitle = "Testing SubTitle"
            //};
            //startPoint.SetIcon(Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon");
            //viewModel.Pins.Add(startPoint);

            //var endPoint = new PointAnnotation()
            //{
            //    Id = "1234",
            //    Coordinate = new Position(10.321897, 123.976114),
            //    Title = "Testing Title",
            //    SubTitle = "Testing SubTitle"
            //};
            //startPoint.SetIcon(Device.RuntimePlatform == Device.Android ? "icon" : "AppIcon");
            //viewModel.Pins.Add(endPoint);

            //Map.MoveCamera(new CoordinateBounds(new Position(10.337510, 123.930414), new Position(10.321897, 123.976114)), new Thickness(25, 25, 25, 25), true);
            //Map.MoveToRegion(new CameraSpan(new Position(10.337510, 123.930414), 90, 100 , 50));
        }
    }
}
