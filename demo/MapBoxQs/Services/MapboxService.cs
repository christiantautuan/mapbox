﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using ModernHttpClient;
using Naxam.Controls.Mapbox.Forms;
using Newtonsoft.Json;
using System.Collections.Generic;
using Naxam.Mapbox.Forms;

namespace MapBoxQs.Services
{
	public interface IMapBoxService
    {
        Task<MapStyle[]> GetAllStyles();
        Task<MapStyle> GetStyleDetails(string id, string owner = null);
        Task<MapDirection> GetDirection(Position startPosition, Position endPosition, DirectionMode directionMode = DirectionMode.Driving);
        Task<MapPlaces> SearchPlace(string searchPlace, bool autocomplete = true, string country = "ph");
        Task<MapPlaces> SearchPlaceFromPosition(Position searchPlace, bool autocomplete = true, string country = "ph");

	}

	public class MapBoxService : IMapBoxService
	{
		HttpClient client;

		//private static string BaseURL = "https://api.mapbox.com/";
		//public static string AccessToken = "sk.eyJ1IjoibmF4YW10ZXN0IiwiYSI6ImNqNWtpb2d1ZzJpMngyd3J5ZnB2Y2JhYmQifQ.LEvGqQkAqM4MO3ZtGbQrdw";
		//public static string Username = "naxamtest";
        //public static string AccessToken = "sk.eyJ1IjoibmF4YW10ZXN0IiwiYSI6ImNqNWtpb2d1ZzJpMngyd3J5ZnB2Y2JhYmQifQ.LEvGqQkAqM4MO3ZtGbQrdw";

        private static string BaseURL = "https://api.mapbox.com";
        private static string GEOCODING_URL = "/geocoding/v5/mapbox.places/";
        //sample geocoding
        //{BaseURL}{GEOCODING_URL }man.json? access_token = pk.eyJ1IjoidGhlLXNlZW5lci05OTkiLCJhIjoiY2prYzZxMmE5MDVlczN2bmtud2gzMmg1MiJ9.- GJ4hW_kQeIuQM_PnifDzw & country = ph & bbox = &autocomplete = true
        //{BaseURL}{GEOCODING_URL }{search}.json? access_token = {AccessToken} & country = {country|2 letter abreviation} &autocomplete = true

        private static string DIRECTION_URL = "/directions/v5";
        //sample direction
        //https://api.mapbox.com/directions/v5/mapbox/cycling/-84.518641,39.134270;-84.512023,39.102779?geometries=geojson&access_token=pk.eyJ1IjoidGhlLXNlZW5lci05OTkiLCJhIjoiY2prYzZxMmE5MDVlczN2bmtud2gzMmg1MiJ9.-GJ4hW_kQeIuQM_PnifDzw
        //{BaseURL}{DIRECTION_URL }{mode}/{start - lat},{start - long};{end lat},{end - long}?geometries=geojson&access_token={AccessToken}
        public static string AccessToken = "sk.eyJ1IjoidGhlLXNlZW5lci05OTkiLCJhIjoiY2prajNvdGZrMTdhMjN2b2dkNHRkZHY2eSJ9.uXx3TrZ3CNUkkN2JyDamFQ";
        public static string Username = "the-seener-999";

		public MapBoxService()
		{
			client = new HttpClient(new NativeMessageHandler())
			{
				MaxResponseContentBufferSize = 256000
			};
		}

        public async Task<MapStyle[]> GetAllStyles()
		{
			var urlFormat = BaseURL + "/styles/v1/{0}?access_token={1}";
			var uri = new Uri(string.Format(urlFormat, Username, AccessToken));
			var response = await client.GetAsync(uri);
			if (response.IsSuccessStatusCode)
			{
				var content = await response.Content.ReadAsStringAsync();
				System.Diagnostics.Debug.WriteLine(content);
				try
                {
                    return JsonConvert.DeserializeObject<MapStyle[]>(content);
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine("[EXCEPTION] " + ex.Message);
				}
			}
			return null;
		}

        public async Task<MapStyle> GetStyleDetails(string id, string owner = null)
		{
			var urlFormat = BaseURL + "/styles/v1/{0}/{1}?access_token={2}";
			var uri = new Uri(string.Format(urlFormat, owner ?? Username, id, AccessToken));
			var response = await client.GetAsync(uri);
			if (response.IsSuccessStatusCode)
			{
				var content = await response.Content.ReadAsStringAsync();
				System.Diagnostics.Debug.WriteLine(content);
				try
				{
                    var output = JsonConvert.DeserializeObject<MapStyle>(content);
					return output;
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine("[EXCEPTION] " + ex.Message);
				}
			}
			return null;
		}

        public async Task<MapPlaces> SearchPlace(string searchPlace, bool autocomplete = true ,string country = "ph")
        {
            var url = $"{BaseURL}{GEOCODING_URL }{searchPlace}.json?access_token={AccessToken}&country={country}&autocomplete={autocomplete}";
            var uri = new Uri(url);
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(content);
                try
                {
                    var output = JsonConvert.DeserializeObject<MapPlaces>(content);
                    return output;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[EXCEPTION] " + ex.Message);
                }
            }
            return null;
        }

        public async Task<MapPlaces> SearchPlaceFromPosition(Position searchPlace, bool autocomplete = true, string country = "ph")
        {
            var url = $"{BaseURL}{GEOCODING_URL }{searchPlace.Long},{searchPlace.Lat}.json?access_token={AccessToken}&country={country}&autocomplete={autocomplete}";
            var uri = new Uri(url);
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(content);
                try
                {
                    var output = JsonConvert.DeserializeObject<MapPlaces>(content);
                    return output;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[EXCEPTION] " + ex.Message);
                }
            }
            return null;
        }

        public async Task<MapDirection> GetDirection(Position startPosition, Position endPosition , DirectionMode directionMode = DirectionMode.Driving)
        {
            var url = $"{BaseURL}{DIRECTION_URL }{directionMode.EnumToString()}/{startPosition.Long},{startPosition.Lat};{endPosition.Long},{endPosition.Lat}?geometries=geojson&access_token={AccessToken}";
            var uri = new Uri(url);
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine(content);
                try
                {
                    var output = JsonConvert.DeserializeObject<MapDirection>(content);
                    return output;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("[EXCEPTION] " + ex.Message);
                }
            }
            return null;
        }

        //public async Tas
	}
}
