﻿
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;
using Naxam.Mapbox.Forms;

namespace Naxam.Controls.Mapbox.Forms
{
    public class PositionChangeEventArgs : EventArgs
    {
        //private Position _newPosition;

        public PositionChangeEventArgs(Position newPosition)
        {
            NewPosition = newPosition;
        }

        public Position NewPosition
        {
            [CompilerGenerated]
            get;
            [CompilerGenerated]
            private set;
        }
    }

    public partial class MapView : View
    {
        public MapView()
        {
            if(string.IsNullOrWhiteSpace(AccessToken) ? true : string.IsNullOrWhiteSpace(AccessToken))
            {
                throw new Exception("static AccessToken must not be null");
            }
        }

        public static string AccessToken { get; set; }

        public string GetAccessToken()
        {
            return AccessToken;
        }
        public static readonly BindableProperty IsMarkerClickedProperty = BindableProperty.Create(
          nameof(IsMarkerClicked),
          typeof(bool),
          typeof(MapView),
          default(bool),
       BindingMode.TwoWay
      );

        public bool IsMarkerClicked
        {
            get
            {
                return (bool)GetValue(IsMarkerClickedProperty);
            }
            set { SetValue(IsMarkerClickedProperty, value); }
        }

		public static readonly BindableProperty DragFinishedCommandProperty = BindableProperty.Create(
			nameof(DidDragFinishedEventCommand),
			typeof(ICommand),
			typeof(MapView),
			default(ICommand),
			BindingMode.OneWay);
		public ICommand DidDragFinishedEventCommand
		{
			get { return (ICommand)GetValue(DragFinishedCommandProperty); }
			set { SetValue(DragFinishedCommandProperty, value); }
		}

		public static readonly BindableProperty FocusPositionProperty = BindableProperty.Create(
            nameof(FocusPosition),
           typeof(bool),
           typeof(MapView),
           default(bool),
            BindingMode.OneWay);


        public bool FocusPosition
        {
            get
            {
                return (bool)GetValue(FocusPositionProperty);
            }
            set { SetValue(FocusPositionProperty, value); }
        }

        public static readonly BindableProperty CenterProperty = BindableProperty.Create(
            nameof(Center),
            typeof(Position),
            typeof(MapView),
            default(Position),
            BindingMode.TwoWay);

        public Position Center
        {
            get
            {
                return (Position)GetValue(CenterProperty);
            }
            set
            {
                SetValue(CenterProperty, (Position)value);
            }
        }

        public static readonly BindableProperty CameraBoundsProperty = BindableProperty.Create(
            nameof(CameraBounds),
            typeof(Bounds),
           typeof(MapView),
            default(Bounds),
           BindingMode.TwoWay);
        
        public Bounds CameraBounds
        {
            get
            {
                return (Bounds)GetValue(CameraBoundsProperty);
            }
            set
            {
                SetValue(CameraBoundsProperty, (Bounds)value);
            }
        }

        public static readonly BindableProperty UserLocationProperty = BindableProperty.Create(
            nameof(UserLocation),
            typeof(Position),
            typeof(MapView),
            default(Position),
            BindingMode.OneWayToSource);

        public Position UserLocation
        {
            get
            {
                return (Position)GetValue(UserLocationProperty);
            }
            set
            {
                SetValue(UserLocationProperty, (Position)value);
            }
        }

        public static readonly BindableProperty UserIconProperty = BindableProperty.Create(nameof(UserIcon), typeof(PinIconFactory.PinIcon), typeof(MapView), null);
        [Obsolete("UserIcon i obsolete, use UserPinLocation instead!!!.")]
        public PinIconFactory.PinIcon UserIcon
        {
            get { return (PinIconFactory.PinIcon)GetValue(UserIconProperty); }
            set { SetValue(UserIconProperty, value); }
        }

        public static readonly BindableProperty UserPinLocationProperty = BindableProperty.Create(nameof(UserPinLocation), typeof(UserPin), typeof(MapView), null);

        public UserPin UserPinLocation
        {
            get { return (UserPin)GetValue(UserPinLocationProperty); }
            set { SetValue(UserPinLocationProperty, value); }
        }





        public static BindableProperty ShowUserLocationProperty = BindableProperty.Create(
            propertyName: nameof(ShowUserLocation),
            returnType: typeof(bool),
            declaringType: typeof(MapView),
            defaultValue: default(bool),
            defaultBindingMode: BindingMode.OneWay
        );

        public bool ShowUserLocation
        {
            get { return (bool)GetValue(ShowUserLocationProperty); }
            set { SetValue(ShowUserLocationProperty, value); }
        }

        public static readonly BindableProperty ZoomLevelProperty = BindableProperty.Create(
            nameof(ZoomLevel),
            typeof(double),
            typeof(MapView),
            10.0,
            BindingMode.TwoWay);

        public double ZoomLevel
        {
            get
            {
                return (double)GetValue(ZoomLevelProperty);
            }
            set
            {
                if (Math.Abs(value - ZoomLevel) > 0.01)
                {
                    SetValue(ZoomLevelProperty, (double)value);
                }
            }
        }

        public static readonly BindableProperty PitchProperty = BindableProperty.Create(
            nameof(Pitch),
            typeof(double),
            typeof(MapView),
            0.0,
            BindingMode.TwoWay);

        public double Pitch
        {
            get
            {
                return (double)GetValue(PitchProperty);
            }
            set
            {
                SetValue(PitchProperty, (double)value);
            }
        }

        public static readonly BindableProperty PitchEnabledProperty = BindableProperty.Create(
            nameof(PitchEnabled),
            typeof(bool),
            typeof(MapView),
            default(bool),
            BindingMode.TwoWay);

        public bool PitchEnabled
        {
            get
            {
                return (bool)GetValue(PitchEnabledProperty);
            }
            set
            {
                SetValue(PitchEnabledProperty, value);
            }
        }

        public static readonly BindableProperty ScrollEnabledProperty = BindableProperty.Create(
            nameof(ScrollEnabled),
            typeof(bool),
            typeof(MapView),
            default(bool),
            BindingMode.OneWay);
        public bool ScrollEnabled
        {
            get { return (bool)GetValue(ScrollEnabledProperty); }
            set { SetValue(ScrollEnabledProperty, value); }
        }

        public static readonly BindableProperty RotateEnabledProperty = BindableProperty.Create(
            nameof(RotateEnabled),
            typeof(bool),
            typeof(MapView),
            default(bool),
            BindingMode.TwoWay);

        public bool RotateEnabled
        {
            get
            {
                return (bool)GetValue(RotateEnabledProperty);
            }
            set
            {
                SetValue(RotateEnabledProperty, (bool)value);
            }
        }

        public static readonly BindableProperty RotatedDegreeProperty = BindableProperty.Create(
            nameof(RotatedDegree),
            typeof(double),
            typeof(MapView),
            0.0,
            BindingMode.TwoWay);

        public double RotatedDegree
        {
            get
            {
                return (double)GetValue(RotatedDegreeProperty);
            }
            set
            {
                SetValue(RotatedDegreeProperty, (double)value);
            }
        }

        //public static readonly BindableProperty StyleUrlProperty = BindableProperty.Create(
        //	nameof(StyleUrl),
        //	typeof(string),
        //	typeof(MapView),
        //	default(string));

        //public string StyleUrl
        //{
        //	get
        //	{
        //		return (string)GetValue(StyleUrlProperty);
        //	}
        //	set
        //	{
        //		SetValue(StyleUrlProperty, (string)value);
        //	}
        //}

        public static readonly BindableProperty MapStyleProperty = BindableProperty.Create(
        nameof(MapStyle),
        typeof(MapStyle),
        typeof(MapView),
        default(MapStyle),
            defaultBindingMode: BindingMode.TwoWay);
        public MapStyle MapStyle
        {
            get
            {
                return (MapStyle)GetValue(MapStyleProperty);
            }
            set
            {
                SetValue(MapStyleProperty, value);
            }
        }

        public static readonly BindableProperty AnnotationsProperty = BindableProperty.Create(
            nameof(Annotations),
            typeof(IEnumerable<Annotation>),
            typeof(MapView),
            default(IEnumerable<Annotation>),
            BindingMode.TwoWay);

        public IEnumerable<Annotation> Annotations
        {
            get
            {
                return (IEnumerable<Annotation>)GetValue(AnnotationsProperty);
            }
            set
            {
                SetValue(AnnotationsProperty, (IEnumerable<Annotation>)value);
            }
        }

        public static readonly BindableProperty CanViewPinInfoProperty = BindableProperty.Create(nameof(CanViewPinInfo), typeof(bool), typeof(MapView), false);

        public bool CanViewPinInfo
        {
            get { return (bool)GetValue(CanViewPinInfoProperty); }
            set { SetValue(CanViewPinInfoProperty, value); }
        }

        public event EventHandler<CameraPositionEventArg> DidDragEvent;
        public void SetDidDragEvent(CameraPositionEventArg cameraPositionEventArg)
        {
            DidDragEvent?.Invoke(this, cameraPositionEventArg);
        }

        public event EventHandler<CameraPositionEventArg> DidDragFinished;
        public void SetDidDragFinishedEvent(CameraPositionEventArg cameraPositionEventArg)
        {
            DidDragFinished?.Invoke(this, cameraPositionEventArg);
        }

        public event EventHandler<CameraPositionEventArg> DidDragStarted;
        public void SetDidDragStartedEvent(CameraPositionEventArg cameraPositionEventArg)
        {
            DidDragStarted?.Invoke(this, cameraPositionEventArg);
        }

        public event EventHandler<UserLocationEventArgs> UserLocationChanged;
        public void SetUserLocationChangedEvent(UserLocationEventArgs userLocationEventArgs)
        {
            UserLocationChanged?.Invoke(this, userLocationEventArgs);
        }

        public event EventHandler<PinClickedEventArgs> PinClicked;
        public void SetPinClickedEvent(PinClickedEventArgs pinPositionEventArgs)
        {
            PinClicked?.Invoke(this, pinPositionEventArgs);
        }

    }
}
