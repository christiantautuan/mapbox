﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using Naxam.Mapbox.Forms;
using Naxam.Mapbox.Forms.Models;
using System.Collections.Generic;

namespace Naxam.Controls.Mapbox.Forms
{
   
    public partial class MapView
    {
        public static readonly BindableProperty DidFinishLoadingStyleCommandProperty = BindableProperty.Create (
                    nameof (DidFinishLoadingStyleCommand),
                    typeof(ICommand),
                    typeof (MapView),
                    default (ICommand),
                    BindingMode.OneWay);

        ////[Obsolete("deprecated")]
        /*
         * Output: (MapStyle) style
         */
        public ICommand DidFinishLoadingStyleCommand {
            get {
                return (ICommand)GetValue (DidFinishLoadingStyleCommandProperty);
            }
            set {
                SetValue (DidFinishLoadingStyleCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty DidFinishRenderingCommandProperty = BindableProperty.Create (
                    nameof (DidFinishRenderingCommand),
                    typeof(ICommand),
                    typeof (MapView),
                    default (ICommand),
                    BindingMode.TwoWay);

        ////[Obsolete("deprecated")]
        /*
         * Output: None
         */
        public ICommand DidFinishRenderingCommand {
            get {
                return (ICommand)GetValue (DidFinishRenderingCommandProperty);
            }
            set {
                SetValue (DidFinishRenderingCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty RegionDidChangeCommandProperty = BindableProperty.Create (
                    nameof (RegionDidChangeCommand),
                    typeof(ICommand),
                    typeof (MapView),
                    default (ICommand),
                    BindingMode.TwoWay);

        //[Obsolete("deprecated")]
        /*
         * Output: (bool) animated
         */
        public ICommand RegionDidChangeCommand {
            get {
                return (ICommand)GetValue (RegionDidChangeCommandProperty);
            }
            set {
                SetValue (RegionDidChangeCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty DidTapOnMapCommandProperty = BindableProperty.Create (
            nameof (DidTapOnMapCommand),
            typeof(ICommand),
            typeof (MapView),
            default (ICommand),
            BindingMode.OneWay);
        
        //[Obsolete("deprecated")]
        /// <summary>
        /// Did tap on map
        /// </summary>
        /// <returns>((Position) Tapped location,(Point) Tapped point)</returns>
        public ICommand DidTapOnMapCommand {
            get {
                return (ICommand)GetValue (DidTapOnMapCommandProperty);
            }
            set {
                SetValue (DidTapOnMapCommandProperty, (ICommand)value);
            }
        }

        //[Obsolete("deprecated")]
        /// <summary>
        /// MGLMapViewDelegate -mapView:tapOnCalloutForAnnotation:
        /// </summary>
        /// <returns>Annotation's id</returns>
        public static BindableProperty DidTapOnCalloutViewCommandProperty = BindableProperty.Create(
            propertyName: nameof(DidTapOnCalloutViewCommand),
            returnType: typeof(ICommand),
            declaringType: typeof(MapView),
            defaultValue: default(ICommand),
            defaultBindingMode: BindingMode.OneWay
        );

        public ICommand DidTapOnCalloutViewCommand
        {
            get { return (ICommand)GetValue(DidTapOnCalloutViewCommandProperty); }
            set { SetValue(DidTapOnCalloutViewCommandProperty, value); }
        }

        public static readonly BindableProperty PinClickedCommandProperty = BindableProperty.Create(
            nameof(PinClickedCommand),
            typeof(ICommand),
            typeof(MapView),
            default(ICommand),
            BindingMode.OneWay);

        /// <summary>
        /// Gets or sets the pin clicked command.  [object = PinClickedEventArgs]
        /// 
        /// </summary>
        /// <value>The pin clicked command.</value>
        public ICommand PinClickedCommand
        {
            get
            {
                return (ICommand)GetValue(PinClickedCommandProperty);
            }
            set
            {
                SetValue(PinClickedCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty DidDragEventCommandProperty = BindableProperty.Create(
            nameof(DidDragEventCommand),
            typeof(ICommand),
            typeof(MapView),
            default(ICommand),
            BindingMode.OneWay);

        /// <summary>
        /// Gets or sets the pin clicked command.  [object = PinClickedEventArgs]
        /// 
        /// </summary>
        /// <value>The pin clicked command.</value>
        public ICommand DidDragEventCommand
        {
            get
            {
                return (ICommand)GetValue(DidDragEventCommandProperty);
            }
            set
            {
                SetValue(DidDragEventCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty DidDragStartedCommandProperty = BindableProperty.Create(
            nameof(DidDragStartedCommand),
            typeof(ICommand),
            typeof(MapView),
            default(ICommand),
            BindingMode.OneWay);

        /// <summary>
        /// Gets or sets the pin clicked command.  [object = PinClickedEventArgs]
        /// 
        /// </summary>
        /// <value>The pin clicked command.</value>
        public ICommand DidDragStartedCommand
        {
            get
            {
                return (ICommand)GetValue(DidDragStartedCommandProperty);
            }
            set
            {
                SetValue(DidDragStartedCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty UserLocationChangedCommandProperty = BindableProperty.Create(
            nameof(UserLocationChangedCommand),
            typeof(ICommand),
            typeof(MapView),
            default(ICommand),
            BindingMode.OneWay);

        /// <summary>
        /// Gets or sets the user location changed command.  [object = UserLocationEventArgs]
        /// </summary>
        /// <value>The user location changed command.</value>
        public ICommand UserLocationChangedCommand
        {
            get
            {
                return (ICommand)GetValue(UserLocationChangedCommandProperty);
            }
            set
            {
                SetValue(UserLocationChangedCommandProperty, (ICommand)value);
            }
        }
        //

        /// <summary>
        /// MGLMapViewDelegate -mapView:imageForAnnotation:
        /// </summary>
        /// <returns>(Reusable key, image file name)</returns>
        public static BindableProperty GetImageForAnnotationFuncProperty = BindableProperty.Create(
            propertyName: nameof(GetImageForAnnotationFunc),
            returnType: typeof(Func<string, Tuple<string,string>>),
            declaringType: typeof(MapView),
            defaultValue: default(Func<string, Tuple<string, string>>),
            defaultBindingMode: BindingMode.OneWay
        );

        public Func<string, Tuple<string, string>> GetImageForAnnotationFunc
        {
            get { return (Func<string, Tuple<string, string>>)GetValue(GetImageForAnnotationFuncProperty); }
            set { SetValue(GetImageForAnnotationFuncProperty, value); }
        }

        /// <summary>
        /// Occurs when region did change.
        /// </summary>
        public event EventHandler<bool> RegionDidChange;
        public void SetRegionDidChangeEvent(bool regionDidChangeEventArggs)
        {
            RegionDidChange?.Invoke(this, regionDidChangeEventArggs);
        }

        /// <summary>
        /// Occurs when did finish rendering.
        /// </summary>
        public event EventHandler<bool> DidFinishRendering;
        public void SetDidFinishRenderingEvent(bool didFinishRenderingEventArggs)
        {
            DidFinishRendering?.Invoke(this, didFinishRenderingEventArggs);
        }

        /// <summary>
        /// Occurs when did finish loading style.
        /// </summary>
        public event EventHandler<MapStyle> DidFinishLoadingStyle;
        public void SetDidFinishLoadingStyleEvent(MapStyle didFinishLoadingStyleEventArgs)
        {
            DidFinishLoadingStyle?.Invoke(this, didFinishLoadingStyleEventArgs);
        }

        /// <summary>
        /// Did tap on map
        /// </summary>
        /// <returns>((Position) Tapped location,(Point) Tapped point)</returns>
        public event EventHandler<Pin> DidTapOnMap;
        public void SetDidTapOnMapEvent(Pin pinEventArg)
        {
            DidTapOnMap?.Invoke(this, pinEventArg);
        }

        /// <summary>
        /// MGLMapViewDelegate -mapView:tapOnCalloutForAnnotation:
        /// </summary>
        /// <returns>Annotation's id</returns>
        public event EventHandler<string> DidTapOnCalloutView;
        public void SetDidTapOnCalloutViewEvent(string didTapOnCalloutViewEventArg)
        {
            DidTapOnCalloutView?.Invoke(this, didTapOnCalloutViewEventArg);
        }
        //DidTapOnCalloutViewCommand

        public event Action<Position,Position> ToShowNavigation; 
        public void ShowNavigation(Position start,Position end)
        {
            ToShowNavigation?.Invoke(start,end);
        }

        public Action<CameraSpan> movetToRegion;
        public void MoveToRegion(CameraSpan span)
        {
            movetToRegion?.Invoke(span);
        }

        WeakReference<IMoveCamera> _mapboxDelegate { get; set; }
        public IMoveCamera MapboxDelegate
        {
            get
            {
                IMoveCamera mapboxDelegate;
                return _mapboxDelegate.TryGetTarget(out mapboxDelegate) ? mapboxDelegate : null;
            }

            set
            {
                _mapboxDelegate = new WeakReference<IMoveCamera>(value);
            }
        }

        public Action<CoordinateBounds, Thickness, bool> moveCamera;
        public void MoveCamera(CoordinateBounds bounds, Thickness padding, bool isAnimate = true)
        {
            //moveCamera?.Invoke(bounds, padding, isAnimate);
            MapboxDelegate?.MoveCamera(bounds, padding, isAnimate);
        }

        public void MoveCamera(List<Position> bounds, Thickness padding, bool isAnimate = true)
        {
            //moveCamera?.Invoke(bounds, padding, isAnimate);
            MapboxDelegate?.MoveCamera(bounds, padding, isAnimate);
        }

        //public Action<Position, Position> GetShowNavigation()
        //{
        //    return ToShowNavigation;
        //}

        public void Log(string msg, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            msg = DateTime.Now.ToString("HH:mm:ss tt") + " [MAPBOX]-[" + memberName + "]: " + msg;
            //LogEvent?.Invoke(null, msg);
#if DEBUG
            System.Diagnostics.Debug.WriteLine(msg);
#else
            //DependencyService.Get<ILogServices>().Log(msg);
#endif
        }

    }
}