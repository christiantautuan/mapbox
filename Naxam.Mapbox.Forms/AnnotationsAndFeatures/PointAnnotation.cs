﻿using System;
using Xamarin.Forms;

namespace Naxam.Controls.Mapbox.Forms
{
	public class PointAnnotation: Annotation
	{
		public PointAnnotation()
		{
            //PinIconFactory.
		}

		public EventHandler<string> FinishDragged { get; set; }

        PinIconFactory.PinIcon _Icon;
        public PinIconFactory.PinIcon Icon { get { return _Icon; } set { _Icon = value; OnPropertyChanged(nameof(Icon)); } }


        //public bool? isFromView = null;

        //public string Icon
        //{
        //    get; private set;
        //}

        //public View ViewIcon { get; private set; }
        //public void SetIconFromView(View view)
        //{
        //    ViewIcon = view;
        //    isFromView = true;
        //}

        //public void SetIcon(string iconName)
        //{
        //    Icon = iconName;
        //    isFromView = false;
        //} 

	}

}
