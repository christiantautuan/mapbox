﻿using Xamarin.Forms;

namespace Naxam.Controls.Mapbox.Forms
{
	public class Annotation: Xamarin.Forms.BindableObject
    {
		public Annotation()
		{
		}
        public string HandleId { get; set; }

		public string Id
		{
			get;
			set;
		}

		public string Title
		{
			get;
			set;
		}

		public string SubTitle
		{
			get;
			set;
		}

        public static readonly BindableProperty IsDraggableProperty = BindableProperty.Create(nameof(IsDraggable), typeof(bool), typeof(Annotation), false);

        public bool IsDraggable
        {
            get { return (bool)GetValue(IsDraggableProperty); }
            set { SetValue(IsDraggableProperty, value); }
        }

        public static readonly BindableProperty IsFlatProperty = BindableProperty.Create(nameof(IsFlat), typeof(bool), typeof(Annotation), false);

        public bool IsFlat
        {
            get { return (bool)GetValue(IsFlatProperty); }
            set { SetValue(IsFlatProperty, value); }
        }

        public static readonly BindableProperty RotationProperty = BindableProperty.Create(nameof(Rotation), typeof(double), typeof(Annotation), default(double));

        public double Rotation
        {
            get { return (double)GetValue(RotationProperty); }
            set { SetValue(RotationProperty, value); }
        }

        public static readonly BindableProperty CoordinateProperty = BindableProperty.Create(nameof(Coordinate), typeof(Position), typeof(PointAnnotation), default(Position));
        public Position Coordinate
        {
            get { return (Position)GetValue(CoordinateProperty); }
            set { SetValue(CoordinateProperty, value); }
        }

        //public Point Anchor { get; set; }
        Point _Anchor = new Point(.5,.5);
        public Point Anchor { get { return _Anchor; } set { _Anchor = value; OnPropertyChanged(nameof(Anchor)); } }


	}
}
