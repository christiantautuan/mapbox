﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Naxam.Controls.Mapbox.Forms
{
    public class PolylineAnnotation : Annotation
    {
        public PolylineAnnotation()
        {
        }

        //public Position[] Coordinates
        //{
        //	get;
        //	set;
        //}

        public static readonly BindableProperty CoordinatesProperty = BindableProperty.Create(
                    nameof(Coordinates),
            typeof(IEnumerable<Position>),
                    typeof(PolylineAnnotation),
            default(IEnumerable<Position>),
                    BindingMode.TwoWay);

        public IEnumerable<Position> Coordinates
        {
            get
            {
                return (IEnumerable<Position>)GetValue(CoordinatesProperty);
            }
            set
            {
                SetValue(CoordinatesProperty, (IEnumerable<Position>)value);
            }
        }

        Color _LineColor = Color.Black;
        public Color LineColor { get { return _LineColor; } set { _LineColor = value; OnPropertyChanged(nameof(LineColor)); } }

        double _LineWidth = 5;
        public double LineWidth { get { return _LineWidth; } set { _LineWidth = value; OnPropertyChanged(nameof(LineWidth)); } }



    }
}
