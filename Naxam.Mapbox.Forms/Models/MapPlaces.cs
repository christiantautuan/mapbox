﻿using System;
using System;
using System.Collections.Generic;
using Naxam.Controls.Mapbox.Forms;

namespace Naxam.Controls.Mapbox.Forms
{

    public partial class MapPlaces
    {
        /// <summary>
        /// Gets or sets the type.[FeatureCollection]
        /// </summary>
        /// <value>The type.</value>
        public string type { get; set; }
        /// <summary>
        /// text that is searched
        /// </summary>
        /// <value>The query.</value>
        public List<string> query { get; set; }
        /// <summary>
        /// Gets or sets the features. the search result 
        /// </summary>
        /// <value>The features.</value>
        public List<Feature> features { get; set; }
        public string attribution { get; set; }
    }

    public class Properties
    {
        public string wikidata { get; set; }
    }

    public class PlaceGeometry
    {
        public string type { get; set; }
        public Position position {get {
                if (coordinates != null)
                    if (coordinates.Count > 1)
                        return new Position(coordinates[0], coordinates[1]);
                return new Position(0, 0);
            }}
        public List<double> coordinates { get; set; }
    }

    public class Context
    {
        public string id { get; set; }
        public string short_code { get; set; }
        public string wikidata { get; set; }
        public string text { get; set; }
    }

    public class Feature
    {
        public string id { get; set; }
        public string type { get; set; }
        public List<string> place_type { get; set; }
        public double relevance { get; set; }
        public Properties properties { get; set; }
        public string text { get; set; }
        public string place_name { get; set; }
        public List<double> bbox { get; set; }
        public List<double> center { get; set; }
        public PlaceGeometry geometry { get; set; }
        public List<Context> context { get; set; }
        public string matching_text { get; set; }
        public string matching_place_name { get; set; }
    }

}
