﻿using System;
namespace Naxam.Controls.Mapbox.Forms
{
    public class Bounds
    {
        public Position NorthEast { get; set; }
        public Position NorthWest { get; set; }
        public Position SouthEast { get; set; }
        public Position SouthWest { get; set; }

        public Bounds(Position northEast, Position northWest, Position southEast, Position southWest)
        {
            NorthEast = northEast;
            NorthWest = northWest;
            SouthEast = southEast;
            SouthWest = southWest;

        }
    }

    public class DefaultMapStyle
    {
        public static Naxam.Controls.Mapbox.Forms.MapStyle MapboxStreets { get { return new Naxam.Controls.Mapbox.Forms.MapStyle("mapbox://styles/mapbox/streets-v10"); }}
        public static Naxam.Controls.Mapbox.Forms.MapStyle Outdoors { get { return new Naxam.Controls.Mapbox.Forms.MapStyle("mapbox://styles/mapbox/outdoors-v10"); } }
        public static Naxam.Controls.Mapbox.Forms.MapStyle Light { get { return new Naxam.Controls.Mapbox.Forms.MapStyle("mapbox://styles/mapbox/light-v9"); } }
        public static Naxam.Controls.Mapbox.Forms.MapStyle Dark { get { return new Naxam.Controls.Mapbox.Forms.MapStyle("mapbox://styles/mapbox/dark-v9"); } }
        public static Naxam.Controls.Mapbox.Forms.MapStyle SatelliteStreets { get { return new Naxam.Controls.Mapbox.Forms.MapStyle("mapbox://styles/mapbox/satellite-streets-v10"); } }
    }
}