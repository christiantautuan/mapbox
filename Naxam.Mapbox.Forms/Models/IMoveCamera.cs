﻿using System;
using Xamarin.Forms;
using Naxam.Controls.Mapbox.Forms;
using System.Collections.Generic;
namespace Naxam.Mapbox.Forms.Models
{
    public interface IMoveCamera
    {
        void MoveCamera(CoordinateBounds bounds, Thickness padding, bool isAnimate = true);
        void MoveCamera(List<Position> bounds, Thickness padding, bool isAnimate = true);
    }
}
