﻿using System;
using Xamarin.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;

namespace Naxam.Controls.Mapbox.Forms
{
    public class Pin
    {
        public Position position { get; set; }
        public bool isWater { get; set; }
        public Pin(Position position, bool isWater = false)
        {
            this.position = position;
            this.isWater = isWater;
        }
        //public double? Bearing
    }

    public class UserPin : Annotation
    {
        //public Position position { get; set; }
        //public bool isWater { get; set; }
        PinIconFactory.PinIcon _Icon;
        public PinIconFactory.PinIcon Icon { get { return _Icon; } set { _Icon = value; OnPropertyChanged(nameof(Icon)); } }
        //PinIconFactory
    }

    public abstract class PinIconFactory : Object
    {
        //if(isview)

        public static PinIcon SetIcon(string iconName, double? width = null, double? height = null)
        {
            return new PinIcon().SetIcon(iconName, width, height);
        }
        public static PinIcon SetIcon(View iconView, double? width = null, double? height = null)
        {
            //return new PinIcon()
            return new PinIcon().SetIcon(iconView, width, height);
        }
        public static PinIcon SetIcon(Stream iconView, double? width = null, double? height = null)
        {
            //return new PinIcon()
            return new PinIcon().SetIcon(iconView,width,height);
        }

            public class PinIcon
            {
            public double? Width { get; set; }
            public double? Height { get; set; }
            private View IconView;
            private Stream IconStream;
            private string IconString;
            private int IsIconView { get; set; }
            public PinIcon()
            {
                
            }

            private PinIcon(View view)
            {

            }

            public PinIcon SetIcon(string iconName, double? width = null, double? height = null)
            {
                IsIconView = 1;
                IconString = iconName;
                Height = height;
                Width = width;
                return this;
            }
            public PinIcon SetIcon(View iconView, double? width = null, double? height = null)
            {
                IsIconView = 2;
                IconView = iconView;
                Height = height;
                Width = width;
                return this;
            }

            public PinIcon SetIcon(Stream iconView, double? width = null, double? height = null)
            {
                IsIconView = 3;
                IconStream = iconView;
                Height = height;
                Width = width;
                return this;
            }

            public bool Equals(Type type)
            {
                //if( is string)
                if (type == typeof(string))
                    return true;
                if (type == typeof(View))
                    return true;
                return false;
            }

            public static bool operator ==(PinIcon pinIcon, Type type)
            {
                if (pinIcon is null == false)
                {
                    //if( is string)
                    if (pinIcon.IsIconView == 1 && type == typeof(string))
                    {
                        return true;
                    }
                    else if (pinIcon.IsIconView == 2 && type == typeof(View))
                    {
                        return true;
                    }
                    else if (pinIcon.IsIconView == 3 && type == typeof(Stream))
                    {
                        return true;
                    }
                }
                return pinIcon is null;
            }

            public static bool operator !=(PinIcon pinIcon, Type type)
            {
                if (pinIcon is null == false)
                {
                    //if (type == typeof(string))
                    //    return false;
                    //if (type == typeof(View))
                        //return false;
                    if(pinIcon.IsIconView == 1 && type  == typeof(string))
                    {
                        return false;
                    }
                    else if(pinIcon.IsIconView == 2 && type == typeof(View))
                    {
                        return false;
                    }
                    else if (pinIcon.IsIconView == 3 && type == typeof(Stream))
                    {
                        return false;
                    }
                }
                return !(pinIcon is null);
            }
            //public static implicit operator is(PinIcon pinIcon, Type type)
            //{
            //    return true;
            //}

            public static implicit operator string(PinIcon icon)
            {
                if (icon.IsIconView == 1)
                return icon.IconString;
                
                return null;
            }

            //public static implicit operator is(PinIcon icon, Type str)
            //{
            //    return false;
            //}

            public static implicit operator View(PinIcon icon)
            {
                if (icon.IsIconView == 2)
                    return icon.IconView;

                return null;
            }

            public static implicit operator Stream(PinIcon icon)
            {
                if (icon.IsIconView == 3)
                    return icon.IconStream;

                return null;
            }

        }
    }
}
