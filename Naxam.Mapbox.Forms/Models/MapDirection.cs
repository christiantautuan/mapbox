﻿using System;
using System.Collections.Generic;
using Naxam.Controls.Mapbox.Forms;
using System.Linq;

namespace Naxam.Controls.Mapbox.Forms
{
    public class MapDirection
    {
        public List<Route> routes { get; set; }
        public List<Waypoint> waypoints { get; set; }
        public string code { get; set; }
        public string uuid { get; set; }
    }

    public class Geometry
    {
        public List<List<double>> coordinates { get; set; }
        public List<Position> _ways;
        public List<Position> ways{
            get {
                if (_ways == null)
                {
                    _ways = new List<Position>();

                    if (coordinates != null)
                        foreach (var position in coordinates)
                        {
                            _ways.Add(new Position(position[1], position[0]));
                        }
                }
                return _ways;
            }}
        public string type { get; set; }
    }

    public class Leg
    {
        public string summary { get; set; }
        public double weight { get; set; }
        public double duration { get; set; }
        public List<object> steps { get; set; }
        public double distance { get; set; }
    }

    public class Route
    {
        public Geometry geometry { get; set; }
        public List<Leg> legs { get; set; }
        public string weight_name { get; set; }
        public double weight { get; set; }
        public double duration { get; set; }
        public double distance { get; set; }
    }

    public class Waypoint
    {
        public string name { get; set; }
        public List<double> location { get; set; }
    }

}
