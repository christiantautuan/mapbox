﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using Naxam.Mapbox.Forms;

namespace Naxam.Controls.Mapbox.Forms
{
   
    public partial class NavigationMapView
    {
        public static readonly BindableProperty DidFinishLoadingStyleCommandProperty = BindableProperty.Create (
                    nameof (DidFinishLoadingStyleCommand),
                    typeof (ICommand),
                    typeof (NavigationMapView),
                    default (ICommand),
                    BindingMode.OneWay);

        [Obsolete("deprecated")]
        /*
         * Output: (MapStyle) style
         */
        public ICommand DidFinishLoadingStyleCommand {
            get {
                return (ICommand)GetValue (DidFinishLoadingStyleCommandProperty);
            }
            set {
                SetValue (DidFinishLoadingStyleCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty DidFinishRenderingCommandProperty = BindableProperty.Create (
                    nameof (DidFinishRenderingCommand),
                    typeof (ICommand),
                    typeof (NavigationMapView),
                    default (ICommand),
                    BindingMode.TwoWay);

        [Obsolete("deprecated")]
        /*
         * Output: None
         */
        public ICommand DidFinishRenderingCommand {
            get {
                return (ICommand)GetValue (DidFinishRenderingCommandProperty);
            }
            set {
                SetValue (DidFinishRenderingCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty RegionDidChangeCommandProperty = BindableProperty.Create (
                    nameof (RegionDidChangeCommand),
                    typeof (ICommand),
                    typeof (NavigationMapView),
                    default (ICommand),
                    BindingMode.TwoWay);

        [Obsolete("deprecated")]
        /*
         * Output: (bool) animated
         */
        public ICommand RegionDidChangeCommand {
            get {
                return (ICommand)GetValue (RegionDidChangeCommandProperty);
            }
            set {
                SetValue (RegionDidChangeCommandProperty, (ICommand)value);
            }
        }

        public static readonly BindableProperty DidTapOnMapCommandProperty = BindableProperty.Create (
            nameof (DidTapOnMapCommand),
            typeof (ICommand),
            typeof (NavigationMapView),
            default (ICommand),
            BindingMode.OneWay);
        
        [Obsolete("deprecated")]
        /// <summary>
        /// Did tap on map
        /// </summary>
        /// <returns>((Position) Tapped location,(Point) Tapped point)</returns>
        public ICommand DidTapOnMapCommand {
            get {
                return (ICommand)GetValue (DidTapOnMapCommandProperty);
            }
            set {
                SetValue (DidTapOnMapCommandProperty, (ICommand)value);
            }
        }

        [Obsolete("deprecated")]
        /// <summary>
        /// MGLNavigationMapViewDelegate -mapView:tapOnCalloutForAnnotation:
        /// </summary>
        /// <returns>Annotation's id</returns>
        public static BindableProperty DidTapOnCalloutViewCommandProperty = BindableProperty.Create(
            propertyName: nameof(DidTapOnCalloutViewCommand),
            returnType: typeof(ICommand),
            declaringType: typeof(NavigationMapView),
            defaultValue: default(ICommand),
            defaultBindingMode: BindingMode.OneWay
        );

        public ICommand DidTapOnCalloutViewCommand
        {
            get { return (ICommand)GetValue(DidTapOnCalloutViewCommandProperty); }
            set { SetValue(DidTapOnCalloutViewCommandProperty, value); }
        }

        /// <summary>
        /// MGLNavigationMapViewDelegate -mapView:imageForAnnotation:
        /// </summary>
        /// <returns>(Reusable key, image file name)</returns>
        public static BindableProperty GetImageForAnnotationFuncProperty = BindableProperty.Create(
            propertyName: nameof(GetImageForAnnotationFunc),
            returnType: typeof(Func<string, Tuple<string,string>>),
            declaringType: typeof(NavigationMapView),
            defaultValue: default(Func<string, Tuple<string, string>>),
            defaultBindingMode: BindingMode.OneWay
        );

        public Func<string, Tuple<string, string>> GetImageForAnnotationFunc
        {
            get { return (Func<string, Tuple<string, string>>)GetValue(GetImageForAnnotationFuncProperty); }
            set { SetValue(GetImageForAnnotationFuncProperty, value); }
        }

        /// <summary>
        /// Occurs when region did change.
        /// </summary>
        public event EventHandler<bool> RegionDidChange;
        public void SetRegionDidChangeEvent(bool regionDidChangeEventArggs)
        {
            RegionDidChange?.Invoke(this, regionDidChangeEventArggs);
        }

        /// <summary>
        /// Occurs when did finish rendering.
        /// </summary>
        public event EventHandler<bool> DidFinishRendering;
        public void SetDidFinishRenderingEvent(bool didFinishRenderingEventArggs)
        {
            DidFinishRendering?.Invoke(this, didFinishRenderingEventArggs);
        }

        /// <summary>
        /// Occurs when did finish loading style.
        /// </summary>
        public event EventHandler<MapStyle> DidFinishLoadingStyle;
        public void SetDidFinishLoadingStyleEvent(MapStyle didFinishLoadingStyleEventArgs)
        {
            DidFinishLoadingStyle?.Invoke(this, didFinishLoadingStyleEventArgs);
        }

        /// <summary>
        /// Did tap on map
        /// </summary>
        /// <returns>((Position) Tapped location,(Point) Tapped point)</returns>
        public event EventHandler<Pin> DidTapOnMap;
        public void SetDidTapOnMapEvent(Pin pinEventArg)
        {
            DidTapOnMap?.Invoke(this, pinEventArg);
        }

        /// <summary>
        /// MGLNavigationMapViewDelegate -mapView:tapOnCalloutForAnnotation:
        /// </summary>
        /// <returns>Annotation's id</returns>
        public event EventHandler<string> DidTapOnCalloutView;
        public void SetDidTapOnCalloutViewEvent(string didTapOnCalloutViewEventArg)
        {
            DidTapOnCalloutView?.Invoke(this, didTapOnCalloutViewEventArg);
        }
        //DidTapOnCalloutViewCommand

    }
}