﻿using System;
using Naxam.Controls.Mapbox.Forms;
using Xamarin.Forms;
namespace Naxam.Mapbox.Forms
{
    public class CameraPositionEventArg  : EventArgs
    {
        public CameraPosition PositionCamera { get; set; }
        public int P0 { get; set; }
        //public CameraPositionEventArg()
        //{
        //}
    }

    public class CameraPosition 
    {
        //public Point TargetPoint { get; set; }
        public Position Target { get; set; }
        public double Bearing { get; set; }
        public double Tilt { get; set; }
        public double Zoom { get; set; }
        public CameraPosition()
        {
            
        }
    }

    public class UserLocationEventArgs : EventArgs
    {
        public Position UserLocation { get; set; }
        public UserLocationEventArgs(Position position)
        {
            UserLocation = position;
        }
        public UserLocationEventArgs(double Lat, double Lng)
        {
            UserLocation = new Position(Lat,Lng);
        }
    }

    public class PinClickedEventArgs : EventArgs
    {
        //public Position PinPosition { get; set; }
        public PointAnnotation Pin;
        public PinClickedEventArgs(PointAnnotation Pin)
        {
            this.Pin = Pin;
        }
        //public PinClickedEventArgs(double Lat, double Lng)
        //{
        //    PinPosition = new Position(Lat, Lng);
        //}
    }

    public class CameraSpan
    {
        //public Point TargetPoint { get; set; }
        public Position Center { get; set; }
        public double LatitudeDegress { get; set; }
        public double LongitudeDegress { get; set; }
        public double Altitude { get; set; }
        public double Heading { get; set; }
        public double Pitch { get; set; }
        public CameraSpan(Position center, double Altitude = 0, double Heading =0, double Pitch=0)
        {
            this.Center = center;
            this.Altitude = Altitude;
            this.Heading = Heading;
            this.Pitch = Pitch;
        }
    }

}
