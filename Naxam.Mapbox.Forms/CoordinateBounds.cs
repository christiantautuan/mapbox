﻿using System;
namespace Naxam.Controls.Mapbox.Forms
{
    public class CoordinateBounds
    {
        public CoordinateBounds(Position SouthWest , Position NorthEast)
        {
            this.SouthWest = SouthWest;
            this.NorthEast = NorthEast;
        }

        public CoordinateBounds()
        {
            
        }

        public Position SouthWest
        {
            get;
            set;
        }

        public Position NorthEast
        {
            get;
            set;
        }
    }
}
