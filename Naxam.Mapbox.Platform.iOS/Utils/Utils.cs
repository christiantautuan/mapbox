﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Cryptography;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace Naxam.Mapbox.Platform.iOS
{
    internal static class Utils
    {
        public static UIImage ConvertXamViewToUIImage(View view)
        {
            return ConvertViewToImage(ConvertFormsToNative(view, new CGRect(0, 0, view.WidthRequest, view.HeightRequest)));
        }

        public static UIView ConvertFormsToNative(View view, CGRect size)
        {
            var renderer = Xamarin.Forms.Platform.iOS.Platform.CreateRenderer(view);

            renderer.NativeView.Frame = size;

            renderer.NativeView.AutoresizingMask = UIViewAutoresizing.All;
            renderer.NativeView.ContentMode = UIViewContentMode.ScaleToFill;

            renderer.Element.Layout(size.ToRectangle());

            var nativeView = renderer.NativeView;

            nativeView.SetNeedsLayout();

            return nativeView;
        }

        private static LinkedList<string> lruTracker = new LinkedList<string>();
        private static ConcurrentDictionary<string, UIImage> cache = new ConcurrentDictionary<string, UIImage>();

        public static UIImage ConvertViewToImage(UIView view)
        {
            UIGraphics.BeginImageContextWithOptions(view.Bounds.Size, false, 0);
            view.Layer.RenderInContext(UIGraphics.GetCurrentContext());
            UIImage img = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            // Optimization: Let's try to reuse any of the last 10 images we generated
            var bytes = img.AsPNG().ToArray();
            var md5 = MD5.Create();
            var hash = Convert.ToBase64String(md5.ComputeHash(bytes));

            var exists = cache.ContainsKey(hash);
            if (exists)
            {
                lruTracker.Remove(hash);
                lruTracker.AddLast(hash);
                return cache[hash];
            }
            if (lruTracker.Count > 10) // O(1)
            {
                UIImage tmp;
                cache.TryRemove(lruTracker.First.Value, out tmp);
                lruTracker.RemoveFirst();
            }
            cache.GetOrAdd(hash, img);
            lruTracker.AddLast(hash);
            return img;
        }

        /// <summary>
        /// Gets the page to which an element belongs
        /// </summary>
        /// <returns>The page.</returns>
        /// <param name="element">Element.</param>
         static Page GetParentPage(this VisualElement element)
        {
            if (element != null)
            {
                var parent = element.Parent;
                while (parent != null)
                {
                    if (parent is Page)
                    {
                        return parent as Page;
                    }
                    parent = parent.Parent;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the UIView controller.
        /// </summary>
        /// <returns>The UIV iew controller.</returns>
        /// <param name="element">Element.</param>
        public static UIViewController GetUIViewController(this VisualElement element)
        {
            Page page = element.GetParentPage();
            if (page != null)
            {
                
                var renderer = Xamarin.Forms.Platform.iOS.Platform.GetRenderer(page);
                if (renderer == null)
                {
                    renderer = Xamarin.Forms.Platform.iOS.Platform.CreateRenderer(page);
                    Xamarin.Forms.Platform.iOS.Platform.SetRenderer(page, renderer);
                }
                var viewController = renderer.ViewController;
                if (viewController != null)
                    return viewController;
            }
            return null;
        }

    } // end of static class
}
