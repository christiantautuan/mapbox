﻿using System;
using Mapbox;
using Xamarin.Forms;
namespace Naxam.Mapbox.Platform.iOS.Annotations
{
    public class MBPolyline : MGLPolyline
    {
        public MBPolyline()
        {
            //MBPolyline.PolylineWithCoordinates()
        }
        public Color LineColor { get; set; }
        public double LineWidth { get; set; }
    }
}
