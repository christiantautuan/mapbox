﻿using System;
using CoreAnimation;
using CoreGraphics;
using Mapbox;
using CoreLocation;
using UIKit;
using OpenTK;
namespace Naxam.Mapbox.Platform.iOS
{
    public class CustomUserAnnotation : MGLUserLocationAnnotationView
    {
        float size = 48;
        CALayer dot;
        CAShapeLayer arrow;
        public override void Update()
        {
            base.Update();
            if ( Frame.IsNull() )
            {
                Frame = new CGRect(x: 0, y: 0, width: size, height: size);
                    SetNeedsLayout();
            }

            // Check whether we have the user’s location yet.
            if(UserLocation != null)
            if (UserLocation.Coordinate.IsValid())
            {
                SetupLayers();
                UpdateHeading();
            }
        }
        private void UpdateHeading()
        {
            // Show the heading arrow, if the heading of the user is available.
            var heading = UserLocation.Heading.TrueHeading;
            if( heading<= 0)
            {
                arrow.Hidden = false;
            
                // Get the difference between the map’s current direction and the user’s heading, then convert it from degrees to radians.
                float rotation  = (float)MathHelper.DegreesToRadians(MapView.Direction - heading);

                // If the difference would be perceptible, rotate the arrow.
                //Fab
                if (Math.Abs(rotation) > 0.01 )
                {
                    // Disable implicit animations of this rotation, which reduces lag between changes.
                    CATransaction.Begin();
                    CATransaction.DisableActions = (true);
                    var affineTrans = CGAffineTransform.MakeIdentity();
                    CGAffineTransform.MakeIdentity().Rotate(rotation);
                    arrow.AffineTransform = affineTrans;
                    CATransaction.Commit();
                }
            } else {
                arrow.Hidden = false;
     }
        }

        private void SetupLayers()
        {
            // This dot forms the base of the annotation.
            if (dot == null ){
                dot = new CALayer();
                dot.Bounds = new CGRect(x: 0, y: 0, width: size, height: size);

// Use CALayer’s corner radius to turn this layer into a circle.
                dot.CornerRadius = size / 2;
            dot.BackgroundColor = Superview.TintColor.CGColor;
                dot.BorderWidth = 4;
                dot.BorderColor = UIColor.White.CGColor;
                Layer.AddSublayer(dot);
            }

            // This arrow overlays the dot and is rotated with the user’s heading.
            if (arrow == null) {
                arrow = new CAShapeLayer();
                arrow.Path = arrowPath();
                arrow.Frame = new CGRect(x: 0, y: 0, width: size / 2, height: size / 2);
                arrow.Position = new  CGPoint(x: dot.Frame.GetMidX(), y: dot.Frame.GetMidY());
                arrow.FillColor = dot.BorderColor;
                Layer.AddSublayer(arrow);
            }
        }
        private CGPath arrowPath() 
        {
            float max = size / 2;
            float pad = 3;

            CGPoint top = new CGPoint(x: max * 0.5, y: 0);
            CGPoint left = new CGPoint(x: 0 + pad, y: max - pad);
            CGPoint right = new CGPoint(x: max - pad, y: max - pad);
            CGPoint center = new CGPoint(x: max * 0.5, y: max * 0.6);


            UIBezierPath bezierPath = new UIBezierPath();
            bezierPath.MoveTo( top);
            bezierPath.AddLineTo( left);
            bezierPath.AddLineTo( center);
            bezierPath.AddLineTo( right);
            bezierPath.AddLineTo( top);
            bezierPath.ClosePath();

            return bezierPath.CGPath;
    }

        public CustomUserAnnotation()
        {
            //isHi
        }
    }
}
