﻿using System;
using System.Collections.Generic;
using Foundation;
using Mapbox;
using Naxam.Mapbox.Platform.iOS;
using MapView = Naxam.Controls.Mapbox.Forms.MapView;
using Naxam.Controls.Mapbox.Forms;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Diagnostics;
using Naxam.Mapbox.Forms;
//using MapboxDirections;
//using MapboxCoreNavigation;
//using MapboxNavigation;
using MapboxNavigation;
using Naxam.Mapbox.Forms.Models;
using System.Linq;

namespace Naxam.Controls.Mapbox.Platform.iOS
{
    public partial class MapViewRenderer : IMoveCamera
    {
        //Mapv
        int GestureCounter = 0;



        void SetUpMapViewEvent()
        {
            foreach(var recognizer in MapView.GestureRecognizers)
            {
                System.Diagnostics.Debug.WriteLine(recognizer.ToString());
                //< UIPanGestureRecognizer: 0x7fcaa2c61580; state = Possible; view = < MGLMapView 0x7fcaa412dc00 >; target = < (action = handlePanGesture:, target =< MGLMapView 0x7fcaa412dc00 >)>; must - fail -for = {
                //        < UIPanGestureRecognizer: 0x7fcaa2c665d0; state = Possible; enabled = NO; view = < MGLMapView 0x7fcaa412dc00 >; target = < (action = handleTwoFingerDragGesture:, target =< MGLMapView 0x7fcaa412dc00 >)>>
                //    }>
                //< UIPinchGestureRecognizer: 0x7fcaa2c65850; state = Possible; view = < MGLMapView 0x7fcaa412dc00 >; target = < (action = handlePinchGesture:, target =< MGLMapView 0x7fcaa412dc00 >)>; must - fail -for = {
                //        < UITapGestureRecognizer: 0x6000001ed000; state = Possible; view = < MGLMapView 0x7fcaa412dc00 >; target = < (action = handleTwoFingerTapGesture:, target =< MGLMapView 0x7fcaa412dc00 >)>; numberOfTouchesRequired = 2 >
                //    }>
                //<UIRotationGestureRecognizer: 0x7fcaa2c65e20; state = Possible; enabled = NO; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleRotateGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; must-fail-for = {
                //        <UITapGestureRecognizer: 0x6000001ed000; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleTwoFingerTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; numberOfTouchesRequired = 2>
                //    }>
                //<UITapGestureRecognizer: 0x6000001ecf00; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleDoubleTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; numberOfTapsRequired = 2; must-fail-for = {
                //        <UILongPressGestureRecognizer: 0x7fcaa2c66e00; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleQuickZoomGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>,
                //        <UITapGestureRecognizer: 0x6000001ed100; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleSingleTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>
                //    }>
                //<UIPanGestureRecognizer: 0x7fcaa2c665d0; state = Possible; enabled = NO; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleTwoFingerDragGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; must-fail = {
                //        <UIPanGestureRecognizer: 0x7fcaa2c61580; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handlePanGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>
                //    }; must-fail-for = {
                //        <UITapGestureRecognizer: 0x6000001ed000; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleTwoFingerTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; numberOfTouchesRequired = 2>
                //    }>
                //<UITapGestureRecognizer: 0x6000001ed000; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleTwoFingerTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; numberOfTouchesRequired = 2; must-fail = {
                //        <UIRotationGestureRecognizer: 0x7fcaa2c65e20; state = Possible; enabled = NO; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleRotateGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>,
                //        <UIPanGestureRecognizer: 0x7fcaa2c665d0; state = Possible; enabled = NO; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleTwoFingerDragGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>,
                //        <UIPinchGestureRecognizer: 0x7fcaa2c65850; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handlePinchGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>
                //    }>
                //<UILongPressGestureRecognizer: 0x7fcaa2c66e00; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleQuickZoomGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; must-fail = {
                //        <UITapGestureRecognizer: 0x6000001ecf00; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleDoubleTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; numberOfTapsRequired = 2>
                //    }; must-fail-for = {
                //        <UITapGestureRecognizer: 0x6000001ed100; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleSingleTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>
                //    }>
                //<UITapGestureRecognizer: 0x6000001ed100; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleSingleTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; must-fail = {
                //        <UITapGestureRecognizer: 0x6000001ecf00; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleDoubleTapGesture:, target=<MGLMapView 0x7fcaa412dc00>)>; numberOfTapsRequired = 2>,
                //        <UILongPressGestureRecognizer: 0x7fcaa2c66e00; state = Possible; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=handleQuickZoomGesture:, target=<MGLMapView 0x7fcaa412dc00>)>>
                //    }>
                //<UITapGestureRecognizer: 0x6000001ed300; state = Possible; cancelsTouchesInView = NO; view = <MGLMapView 0x7fcaa412dc00>; target= <(action=target:, target=<__UIGestureRecognizerParametrizedToken 0x60400202d440>)>>

                //var swipeLeft = new UISwipeGestureRecognizer((UISwipeGestureRecognizer obj) =>
                //{
                //    SwipeRightAnimation(e.NewElement);
                //    e.NewElement.TranslateTo(-((e.NewElement.Parent as AbsoluteLayout).Children[0].Width + 0), e.NewElement.Y, 100, Easing.CubicIn);
                //    //gesture.OnTappedEvent( new SwipeEventArgs() { swipeIndication = SwipeIndication.Left });
                //})
                //{ Direction = UISwipeGestureRecognizerDirection.Left };
                GestureCounter++;
                //UIPAnGe
                if (recognizer is UIPanGestureRecognizer uIPan)
                {
                    uIPan.AddTarget((NSObject obj) =>
                    {
                        Debug.WriteLine("Pan");
                        switch(uIPan.State)
                        {
                            case UIGestureRecognizerState.Began:
                                Debug.WriteLine("Pan:Started");
                                System.Diagnostics.Debug.WriteLine($"Map_CameraMove(lat,lang):({MapView.Camera.CenterCoordinate.Latitude},{MapView.Camera.CenterCoordinate.Longitude})");
                                Element?.SetDidDragStartedEvent(new CameraPositionEventArg()
                                {
                                    PositionCamera = new CameraPosition()
                                    {
                                        Target = new Position(MapView.Camera.CenterCoordinate.Latitude, MapView.Camera.CenterCoordinate.Longitude),
                                        Bearing = MapView.Direction,
                                        Tilt = MapView.Camera.Altitude,
                                        Zoom = MapView.ZoomLevel,
                                        //BoundPadding = 
                                    }
                                }); 
                                Element?.DidDragStartedCommand?.Execute(new CameraPositionEventArg()
                                {
                                    PositionCamera = new CameraPosition()
                                    {
                                        Target = new Position(MapView.Camera.CenterCoordinate.Latitude, MapView.Camera.CenterCoordinate.Longitude),
                                        Bearing = MapView.Direction,
                                        Tilt = MapView.Camera.Altitude,
                                        Zoom = MapView.ZoomLevel,
                                        //BoundPadding = 
                                    }
                                });
                                break;
                            case UIGestureRecognizerState.Ended:
                                Debug.WriteLine("Pan:Ended");
                                break;
                        }
                        //obj.

                        //MapView.Camera.ce
                    });
                }
                //else if (recognizer is UIPinchGestureRecognizer uIPinch)
                //{
                //    uIPinch.AddTarget((NSObject obj) =>
                //   {
                //       //obj.
                //       Debug.WriteLine("PINCH");
                //       //MapView.Camera.ce
                //   });
                //}
                //else if (recognizer is UIRotationGestureRecognizer uIRotation)
                //{
                //        uIRotation.AddTarget((NSObject obj) =>
                //        {
                //        //obj.
                //        Debug.WriteLine("Rotation");
                //        //MapView.Camera.ce
                //        });  
                //}
                //else if (recognizer is UITapGestureRecognizer uITap)
                //{
                //        uITap.AddTarget((NSObject obj) =>
                //        {
                //        //obj.
                //        Debug.WriteLine("Tap");
                //            //MapView.Camera.ce    
                //            });  
                //}
                //else if (recognizer is UILongPressGestureRecognizer uILong)
                //{
                //        uILong.AddTarget((NSObject obj) =>
                //        {
                //        //obj.
                //        Debug.WriteLine("Long");
                //            //MapView.Camera.ce
                //            });  
                //}


            }// end of loop
            Debug.WriteLine($"Gesture Count: {GestureCounter}");

            Element.Log("Gesture Added");
            //var showNavigationEvent = Element.GetShowNavigation();
            Element.ToShowNavigation += ShowNavigation;
            Element.MapboxDelegate = this;
        }//end of SetUpMapViewEvent

        public void MoveCamera(CoordinateBounds bounds, Thickness padding, bool isAnimate = true)
        {
            MapView.SetVisibleCoordinateBounds(new MGLCoordinateBounds() { ne = bounds.NorthEast.ToCLCoordinate(), sw = bounds.SouthWest.ToCLCoordinate() }, new UIEdgeInsets((nfloat)padding.Top, (nfloat)padding.Left, (nfloat)padding.Bottom, (nfloat)padding.Right), isAnimate);
        }

        public void MoveCamera(List<Position> bounds, Thickness padding, bool isAnimate = true)
        {
            //MapView.Annotations.Where((IMGLAnnotation arg) => arg.Coordinate == bounds[0].ToCLCoordinate()  );
            //MapView.ShowAnnotations()
            MapView.SetVisibleCoordinateBounds(bounds.ToBounds(), new UIEdgeInsets((nfloat)padding.Top, (nfloat)padding.Left, (nfloat)padding.Bottom, (nfloat)padding.Right), isAnimate);
        }

        void ShowNavigation(Position start, Position end)
        {
            //MGLStyle.StreetsStyleURL
            //Com.Mapbox.Mapboxsdk.Constants.Style
            //MGLStyle.
            //MBStyle.
            //var viewController = Element.GetUIViewController();

            //CoreLocation.CLLocationCoordinate2D startingPoint = new CoreLocation.CLLocationCoordinate2D(start.Lat, start.Long);
            //CoreLocation.CLLocationCoordinate2D endingPoint = new CoreLocation.CLLocationCoordinate2D(end.Lat, end.Long);

            //var origin = new MBWaypoint((CoreLocation.CLLocationCoordinate2D)startingPoint, -1,"Your location");

            //var destination = new MBWaypoint((CoreLocation.CLLocationCoordinate2D)endingPoint,-1,"Destination");
            //var options = new MBNavigationRouteOptions(
            //    new MBWaypoint[] { origin, destination },
            //    MBDirectionsProfileIdentifier.AutomobileAvoidingTraffic
            //);

            //MBDirections.SharedDirections.CalculateDirectionsWithOptions(
            //options,
            //(waypoints, routes, error) =>
            //{
            //    if (routes == null || routes.Length == 0)
            //    {
            //        Element.Log("No routes found");
            //        //string errorMessage = "No routes found";
            //        //if (error != null)
            //        //{
            //        //    errorMessage = error.LocalizedDescription;
            //        //}
            //        //var alert = UIAlertController.Create("Error", errorMessage, UIAlertControllerStyle.Alert);
            //        //alert.AddAction(UIAlertAction.Create("Dismiss", UIAlertActionStyle.Cancel, null));
            //        //PresentViewController(alert, true, null);
            //        //viewController.PresentViewController(alert, true, null);
            //    }
            //    else
            //    {
            //        var locationManager = new MBSimulatedLocationManager(routes[0]);

            //        try
            //        {
            //            //var mbViewController = new MBNavigationViewController(routes[0], MBDirections.SharedDirections, null, locationManager);
            //            //var mbViewController = new UIViewController();
            //            //mbViewController.View = new UIView() { BackgroundColor = UIColor.Red };
            //            //viewController.PresentViewController(mbViewController, true, null);
            //        }
            //        catch( Exception e)
            //        {
            //            Element.Log(e.Message);
            //        }

            //        //PresentViewController(viewController, true, null);
            //        //MapView.Prese
            //    }
            //});
            ////System.Diagnostics.Debug.WriteLine(Show)
            //Element.Log("ShowNavigation");
        }

        //private void DidTapOnRoutingBtn(object sender, EventArgs e)
        //{
        //    var origin = new MBWaypoint((CoreLocation.CLLocationCoordinate2D)userCoordinate,
        //    -1,
        //    "Your location");

        //    var destination = new MBWaypoint((CoreLocation.CLLocationCoordinate2D)destinationCoordinate,
        //                                -1,
        //                                     "Destination");
        //    var options = new MBNavigationRouteOptions(
        //        new MBWaypoint[] { origin, destination },
        //        MBDirectionsProfileIdentifier.AutomobileAvoidingTraffic
        //    );

        //    MBDirections.SharedDirections.CalculateDirectionsWithOptions(
        //    options,
        //    (waypoints, routes, error) =>
        //    {
        //        if (routes == null || routes.Length == 0)
        //        {
        //            string errorMessage = "No routes found";
        //            if (error != null)
        //            {
        //                errorMessage = error.LocalizedDescription;
        //            }
        //            var alert = UIAlertController.Create("Error", errorMessage, UIAlertControllerStyle.Alert);
        //            alert.AddAction(UIAlertAction.Create("Dismiss", UIAlertActionStyle.Cancel, null));
        //            //PresentViewController(alert, true, null);
        //        }
        //        else
        //        {
        //            var locationManager = new MBSimulatedLocationManager(routes[0]);

        //            var viewController = new MBNavigationViewController(routes[0], MBDirections.SharedDirections, null, locationManager);
        //            //PresentViewController(viewController, true, null);
        //            //MapView.Prese
        //        }
        //    });
        //}

    }
}
