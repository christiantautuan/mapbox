﻿using System;
using System.Collections.Generic;
using CoreLocation;
using Mapbox;
using Naxam.Controls.Mapbox.Forms;

namespace Naxam.Controls.Mapbox.Platform.iOS
{
    public static class PositionExtensions
    {
        public static CLLocationCoordinate2D ToCLCoordinate(this Position pos)
        {
            return new CLLocationCoordinate2D(pos.Lat, pos.Long);
        }

        public static bool Equals(this CLLocationCoordinate2D cL, Position pos)
        {
            if(Math.Abs(cL.Latitude - pos.Lat) < double.Epsilon)
            {
                
            }
            return false;
        }

        public static MGLCoordinateBounds ToBounds(this List<Position> bounds)
        {

            if(bounds == null)
            {
                return new MGLCoordinateBounds();
            }
            double minLat = 90;
            double minLon = 180;
            double maxLat = -90;
            double maxLon = -180;

            foreach(Position pos in bounds)
            {
                double latitude = pos.Lat;
                double longitude = pos.Long;

                minLat = Math.Min(minLat, latitude);
                minLon = Math.Min(minLon, longitude);
                maxLat = Math.Max(maxLat, latitude);
                maxLon = Math.Max(maxLon, longitude);
                System.Diagnostics.Debug.WriteLine($"Min(Lat,Long) =({minLat},{minLon}) \nMax(Lat,Long) = ({maxLat},{maxLon})");
            }
            //new MGLCoordinateBounds()

            //return new LatLngBounds(maxLat, maxLon, minLat, minLon);
            return new MGLCoordinateBounds() { ne = new CLLocationCoordinate2D(maxLat, maxLon), sw = new CLLocationCoordinate2D(minLat, minLon) };
        }
    }
}
