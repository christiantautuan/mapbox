﻿using System;
using Xamarin.Forms.Platform.iOS;
using MapboxNavigation;
using Naxam.Controls.Mapbox.Forms;
using Naxam.Controls.Mapbox.Platform.iOS;
using Mapbox;

[assembly: Xamarin.Forms.ExportRenderer(typeof(Naxam.Controls.Mapbox.Forms.NavigationMapView), typeof(MapViewRenderer))]
namespace Naxam.Controls.Mapbox.Platform.iOS
{
    public class NavigationMapRenderer : ViewRenderer<Naxam.Controls.Mapbox.Forms.NavigationMapView, MBNavigationMapView>
    {
        MBNavigationMapView mapView;
        protected override void OnElementChanged(ElementChangedEventArgs<NavigationMapView> e)
        {
            base.OnElementChanged(e);
        }

        //public NavigationMapRenderer()
        //{
        //}
    }
}
