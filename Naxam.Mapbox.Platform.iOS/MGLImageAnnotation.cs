﻿using System;
using CoreLocation;
using Mapbox;
using UIKit;
using Xamarin.Forms;
namespace Naxam.Mapbox.Platform.iOS
{
    public class MGLImageAnnotation : MGLPointAnnotation
    {
        //public CLLocationCoordinate2D Coordinate { get; set;}

        //public IntPtr Handle { get; set;}

        public string Id { get; set; }

        //public string Title { get; set; }

        public string SubTitle { get; set; }

        //public UIImage Image { get; set; }

        public UIView Image { get; set; }

        public Point Anchor { get; set; }

        public double Rotation { get; set; }

        public bool IsFlat { get; set; }

        public bool IsDraggable { get; set; }

        //public void Dispose()
        //{
        //    Coordinate = (default(CLLocationCoordinate2D));
        //    Handle = default(IntPtr);
        //    Id = null;
        //    Image = null;
        //}
        //public CLLocationCoordinate2D Coordinate { get; set; }
    }

    public class MGLUserImageAnnotation : MGLImageAnnotation
    {
        public MGLUserImageAnnotation()
        {

        }
    }
}
