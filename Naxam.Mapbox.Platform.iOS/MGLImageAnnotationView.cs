﻿using System;
using CoreGraphics;
using Mapbox;
using UIKit;
using Naxam.Controls.Mapbox.Forms;
using Naxam.Controls.Mapbox.Platform.iOS;
using Foundation;
using Xamarin.Forms;

namespace Naxam.Mapbox.Platform.iOS
{
    public class MGLImageAnnotationView : MGLAnnotationView 
    {
        //public Position position { get; set; }
        public PointAnnotation Pin { get; set; }
        public event EventHandler DragFinished;
        public MGLImageAnnotation PinAnnotation;
        public MGLImageAnnotationView(string reussableID,UIView uIImage,Point anchor , double rotation) : base(reussableID)
        {
            //this.Layer.ContentsCenter
            //this.Layer.BackgroundColor = UIColor.Red.CGColor;
            Draggable = true;

            ScalesWithViewingDistance = false;

            uIImage.Frame = new CGRect(0, 0, uIImage.Frame.Width, uIImage.Frame.Height > 5 ? uIImage.Frame.Height : 40 );
            this.Frame = uIImage.Frame;
            //this.CenterOffset = new CGVector(uIImage.Frame.Width / 2, uIImage.Frame.Height / 2);
            this.Add(uIImage);
            var tapGest = new UITapGestureRecognizer();
            tapGest.NumberOfTapsRequired = 1;
            tapGest.CancelsTouchesInView = false;
            //tapGest.Delegate = this;
            tapGest.AddTarget((NSObject) =>
            {
                //MapViewRenderer.isAnnotationClick = true;
                if (MapViewRenderer.This?.Element.CanShowCalloutChecker?.Invoke(Pin.Id) == true)
                {
                    MapViewRenderer.This?.MapView_TapOnCalloutForAnnotation(MapViewRenderer.MapView, PinAnnotation);
                }
                MapViewRenderer.This?.Element.SetPinClickedEvent(new Forms.PinClickedEventArgs(Pin));
            });
            AddGestureRecognizer(tapGest);
            //Layer
            //MGLIconPitchAlignment
            //RotatesToMatchCamera = true;
            //this.Layer.ZPosition = 100000;
            //Rota
            //CenterOffset = new CGVector()
            //this.Annotation.
            CenterOffset = new CGVector((nfloat)anchor.X, (nfloat)anchor.Y);
            Layer.AffineTransform = CGAffineTransform.MakeRotation((nfloat)rotation);
            //Layer.AnchorPoint = new CGPoint();
        }

        public override UIView HitTest(CGPoint point, UIEvent uievent)
        {
            var hitview = base.HitTest(point, uievent);
            if(hitview == this)
            {
                //MapViewRenderer.MapView.HitTest(point, uievent);
                MapViewRenderer.isAnnotationClick = true;
                System.Diagnostics.Debug.WriteLine($"[IconHitTest]: {this} | POINT(lat,lng) : ({point.X},{point.Y})");
                this.Superview?.BringSubviewToFront(this);
                //MapViewRenderer.This?.Element.SetPinClickedEvent(new Forms.PinClickedEventArgs(Pin));
                //return null;
                return hitview;
            }
            return hitview;
        }

        public override void SetDragState(MGLAnnotationViewDragState dragState, bool animated)
        {
            base.SetDragState(dragState, animated);
            switch (dragState)
            {
                case MGLAnnotationViewDragState.Starting:
                    StartDragging();
                    break;
                case MGLAnnotationViewDragState.Dragging:
                    break;
                case MGLAnnotationViewDragState.Ending:
                case MGLAnnotationViewDragState.Canceling:
                    EndDragging();
                    break;
                default:
                    break;
            }
        }

        private void EndDragging()
        {
            Transform = CGAffineTransform.MakeIdentity();
            Transform.Scale((nfloat)3, (nfloat)3);
            UIView.Animate(duration: 0.3, animation: () =>
            {
                Layer.Opacity = 0.8f;
                Transform.Scale((nfloat)1, (nfloat)1);
            }, completion: null);
            DragFinished.Invoke(this, null);
        }

        private void StartDragging()
        {
            UIView.Animate(duration: 0.3, animation: () =>
            {
                Layer.Opacity = 0.8f;
                Transform = CGAffineTransform.MakeIdentity();
                Transform.Scale((nfloat)3, (nfloat)3);
            }, completion: null);
        }
    }
}
